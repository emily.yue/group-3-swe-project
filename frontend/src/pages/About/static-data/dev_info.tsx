import Charlotte from "./Headshots/CS_Headshot.jpg";
import Emily from "./Headshots/EY_Headshot.jpg";
import Jeffrey from "./Headshots/unnamed.jpg";
import Aribah from "./Headshots/Aribah_Headshot.jpeg";
import Yash from "./Headshots/YS_Headshot.jpeg";

const DevInfo = [
    {
        image: Aribah,
        name: "Aribah Hoque",
        git_user: "aribah09",
        role: "Frontend Developer",
        bio: "I am a junior at UT Austin pursuing a Computer Science degree. I enjoy working out, traveling, and spending time with my friends and family.",
        email: "aribah.hoque@icloud.com",
        tests: 0
    },

    {
        image: Yash,
        name: "Yash Saxena",
        git_user: "saxenaya",
        role: "Backend Developer",
        bio: "I'm a junior at UT Austin pursuing a BS in Computer Science. My interests include running, rock climbing, playing board games, and cooking for friends and family.",
        email: "yash.saxena@utexas.edu",
        tests: 0
    },

    {
    image: Jeffrey,
    name: "Jeffrey Jiang",
    git_user: "AnonymousAmalgrams",
    role: "Backend Developer",
    bio: "I'm a junior at UT Austin pursuing a BS in Computer Science with a Quantum Information Science certificate and a minor in Economics. My interests include strategy and gacha games, reading, military history, and finance.",
    email: "jeffreyj2314@gmail.com",
    tests: 0
    },

    {
        image: Charlotte,
        name: "Charlotte Stinson",
        git_user: "charlottestinson",
        role: "Frontend Developer",
        bio: "I'm a junior at UT pursuing degrees in Computer Science and Plan II Honors. I enjoy baking, playing piano, and going to UT football games.",
        email: "charlottestinson@utexas.edu",
        tests: 0
    }
    
];

export { DevInfo };
