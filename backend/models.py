from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy import String, ForeignKey, Table, Column
from typing import List
from sqlalchemy.orm import relationship

class Base(DeclarativeBase):
    pass

association_table = Table(
    "association_table",
    Base.metadata,
    Column("coal_mine_id", ForeignKey("coalmines.idx")),
    Column("healthcare_provider_id", ForeignKey("healthcare_providers.idx")),
)

class County(Base):
    __tablename__ = "counties"
    __table_args__ = {"mysql_engine": "InnoDB"}

    idx : Mapped[int] = mapped_column(primary_key=True)
    median_income : Mapped[float]
    population : Mapped[int]
    state : Mapped[str] = mapped_column(String(100))
    county : Mapped[str] = mapped_column(String(200))
    fips : Mapped[str] = mapped_column(String(100))
    close_coal_mines : Mapped[str] = mapped_column(String(1000))
    close_healthcare_providers : Mapped[str] = mapped_column(String(1000))

    def __repr__(self):
        return f"County = {self.county}"
    
    def to_dict(self):
        return {
            "idx" : self.idx,
            "median_income" : self.median_income,
            "population" : self.population,
            "state" : self.state,
            "county" : self.county,
            "fips" : self.fips,
            "close_coal_mines" : self.close_coal_mines,
            "close_healthcare_providers" : self.close_healthcare_providers
        }

class CoalMine(Base):
    __tablename__ = "coalmines"
    __table_args__ = {"mysql_engine": "InnoDB"}


    idx : Mapped[int] = mapped_column(primary_key=True)
    mineName : Mapped[str] = mapped_column(String(200))
    coalRankDescription : Mapped[str] = mapped_column(String(200))
    average_employees : Mapped[int]
    mineStatusDescription : Mapped[str] = mapped_column(String(200))
    production : Mapped[int]
    latitude : Mapped[float]
    longitude : Mapped[float]

    close_county : Mapped[int]
    close_healthcare_providers : Mapped[str] = mapped_column(String(1000))

    def __repr__(self):
        return f"CoalMine = {self.mineName}"
    
    def to_dict(self):
        return {
            "idx" : self.idx,
            "mineName" : self.mineName,
            "coalRankDescription" : self.coalRankDescription,
            "average_employees" : self.average_employees,
            "mineStatusDescription" : self.mineStatusDescription,
            "production" : self.production,
            "latitude" : self.latitude,
            "longitude" : self.longitude,
            "close_county" : self.close_county,
            "close_healthcare_providers" : self.close_healthcare_providers
        }

class HealthcareProvider(Base):
    __tablename__ = "healthcare_providers"
    __table_args__ = {"mysql_engine": "InnoDB"}

    idx : Mapped[int] = mapped_column(primary_key=True)
    name : Mapped[str] = mapped_column(String(200))
    street_address : Mapped[str] = mapped_column(String(200))
    hospital_bed_count : Mapped[int]
    chrch_affl_f : Mapped[str] = mapped_column(String(100))
    medicare_provider_number : Mapped[str] = mapped_column(String(100))
    full_address : Mapped[str] = mapped_column(String(200))
    hospital_id : Mapped[int]

    close_county : Mapped[int]
    close_coal_mines : Mapped[str] = mapped_column(String(1000))

    def __repr__(self):
        return f"Healthcare Provider = {self.name}"
    
    def to_dict(self):
        return {
            "idx" : self.idx,
            "name" : self.name,
            "street_address" : self.street_address,
            "hospital_bed_count" : self.hospital_bed_count,
            "church_affl_f" : self.church_affl_f,
            "medicare_provider_number" : self.medicare_provider_number,
            "full_address" : self.full_address,
            "hospital_id" : self.hospital_id,
            "close_county" : self.close_county,
            "close_coal_mines" : self.close_coal_mines
        }