from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from sqlalchemy.sql import text, func
from difflib import SequenceMatcher

import unittest
from models import County, CoalMine, HealthcareProvider
from ast import literal_eval

def create_sql_engine():
    user = "admin"
    password = "coalReality123!"
    url = "coal-reality-4.cfpvhlsbmqce.us-east-1.rds.amazonaws.com"
    port = "3306"
    database = "coal_reality_4"
    mysql_url = f"mysql+mysqlconnector://{user}:{password}@{url}:{port}/{database}"

    engine = create_engine(mysql_url)
    return engine

county_attr_map = {
    "idx" : [County.idx, int],
    "median_income" : [County.median_income, float],
    "population" : [County.population, int],
    "state" : [County.state, str],
    "county" : [County.county, str],
    "fips" : [County.fips, int]
}

coal_mine_attr_map = {
    "idx" : [CoalMine.idx, int],
    "mineName" : [CoalMine.mineName, str],
    "coalRankDescription" : [CoalMine.coalRankDescription, str],
    "average_employees" : [CoalMine.average_employees, float],
    "mineStatusDescription" : [CoalMine.mineStatusDescription, str],
    "production" : [CoalMine.production, float],
    "latitude" : [CoalMine.latitude, float],
    "longitude" : [CoalMine.longitude, float],
    "close_county" : [CoalMine.close_county, int]
}

healthcare_provider_attr_map = {
    "idx" : [HealthcareProvider.idx, int],
    "name" : [HealthcareProvider.name, str],
    "street_address" : [HealthcareProvider.street_address, str],
    "hospital_bed_count" : [HealthcareProvider.hospital_bed_count, int],
    "chrch_affl_f" : [HealthcareProvider.chrch_affl_f, str],
    "medicare_provider_number" : [HealthcareProvider.medicare_provider_number, str],
    "full_address" : [HealthcareProvider.full_address, str],
    "hospital_id" : [HealthcareProvider.hospital_id, int],
    "close_county" : [HealthcareProvider.close_county, int]
}

def sort_by(request, d_l):
    if "sort_by" in request:
        d_l.sort(key=lambda d: d[request["sort_by"]])
    return d_l

def apply_filter(query, filter, attr_map):
    assert len(filter) == 2
    if len(filter[1]) == 0:
        # Strict inequality
        if ">" in filter[0]:
            column, value = filter[0].split(">")
            class_column, class_type = attr_map[column]
            query = query.filter(class_column > class_type(value))
        elif "<" in filter[0]:
            column, value = filter[0].split("<")
            class_column, class_type = attr_map[column]
            query = query.filter(class_column < class_type(value))
        else:
            assert False
    else:
        # Equality, lte, or gte
        if ">" in filter[0]:
            column, value = filter[0][:-1], filter[1]
            class_column, class_type = attr_map[column]
            query = query.filter(class_column >= class_type(value))
        elif "<" in filter[0]:
            column, value = filter[0][:-1], filter[1]
            class_column, class_type = attr_map[column]
            query = query.filter(class_column <= class_type(value))
        else:
            column, value = filter
            class_column, class_type = attr_map[column]
            query = query.filter(class_column == class_type(value))
    return query

def match_string_score(search_string, value):
    return search_string in str(value)

def match_score(search_string, instance):
    return sum(match_string_score(search_string, value) for key, value in instance.items() if "close" not in key)

def get_match_string(search_string, instance):
    best_string = None
    best_score = -1
    for key, value in instance.items():
        if "close" in key:
            continue
        score = match_string_score(search_string, value)
        if score >= best_score:
            best_score = score
            best_string = value
    return str(best_string)

def apply_search(args, instances):
    if "search" not in args:
        return instances
    search_string = args["search"]
    instances.sort(key=lambda x: match_score(search_string, x), reverse=True)
    match_strings = [get_match_string(search_string, instance) for instance in instances]

    for instance, match_str in zip(instances, match_strings):
        instance["match_str"] = match_str
    
    return instances

def apply_filters(query, filters, attr_map):
    for filter in filters:
        if filter[0] in {"sort_by", "search"}:
            continue
        if filter[0] in ["close_healthcare_providers", "close_coal_mines"]:
            continue
        query = apply_filter(query, filter, attr_map)
    return query

def find_matches(str_list, str2):
    matches = []
    for str1 in str_list:
        matches += find_largest_common_substrings(str1, str2)
    matches.sort(key=lambda x: len(x), reverse=True)
    return [matches[0]]

def find_largest_common_substrings(str1, str2):
    matcher = SequenceMatcher(None, str1.lower(), str2.lower())
    ans = []
    for match in matcher.get_matching_blocks():
        if match and match.size > 0:
            ans.append(str1[match.a: match.a + match.size])
    return ans

def get_all_counties(filters=[]):
    engine = create_sql_engine()
    with Session(engine) as session:
        print("Filter:", filters)
        query = session.query(County)
        query = apply_filters(query, filters, county_attr_map)
        counties = query.all()

        for filter in filters:
            if filter[0] == "close_healthcare_providers":
                counties = [c for c in counties if int(filter[1]) in literal_eval(c.close_healthcare_providers)]
            if filter[0] == "close_coal_mines":
                counties = [c for c in counties if int(filter[1]) in literal_eval(c.close_coal_mines)]
    engine.dispose()
    return counties

def get_specific_county(idx):
    "Returns None if idx is invalid"
    engine = create_sql_engine()
    with Session(engine) as session:
        county = session.query(County).filter_by(idx=idx).first()
    engine.dispose()
    return county

def get_cmatch(prompt, limval):
    engine = create_sql_engine()
    with Session(engine) as session:
        search = text("MATCH (county, fips, state) AGAINST (:prompt)"
            ).bindparams(prompt=prompt)
        retval = session.query(County).filter(search).limit(limval)
    engine.dispose()
    return retval

def get_all_coal_mines(filters=[]):
    engine = create_sql_engine()
    with Session(engine) as session:
        query = session.query(CoalMine)
        query = apply_filters(query, filters, coal_mine_attr_map)
        coal_mines = query.all()

        for filter in filters:
            if filter[0] == "close_healthcare_providers":
                coal_mines = [c for c in coal_mines if int(filter[1]) in literal_eval(c.close_healthcare_providers)]
    engine.dispose()
    return coal_mines

def get_specific_coal_mine(idx):
    "Returns None if idx is invalid"
    engine = create_sql_engine()
    with Session(engine) as session:
        coal_mine = session.query(CoalMine).filter_by(idx=idx).first()
    engine.dispose()
    return coal_mine

def get_cmmatch(prompt, limval):
    engine = create_sql_engine()
    with Session(engine) as session:
        search = text("MATCH (mineName, coalRankDescription, mineStatusDescription) AGAINST (:prompt)"
            ).bindparams(prompt=prompt)
        retval = session.query(CoalMine).filter(search).limit(limval)
    engine.dispose()
    return retval

def get_all_healthcare_providers(filters=[]):
    engine = create_sql_engine()
    with Session(engine) as session:
        query = session.query(HealthcareProvider)
        query = apply_filters(query, filters, healthcare_provider_attr_map)
        healthcare_providers = query.all()

        for filter in filters:
            if filter[0] == "close_coal_mines":
                healthcare_providers = [hp for hp in healthcare_providers if int(filter[1]) in literal_eval(hp.close_healthcare_providers)]
    engine.dispose()
    return healthcare_providers

def get_specific_healthcare_provider(idx):
    "Returns None if idx is invalid"
    engine = create_sql_engine()
    with Session(engine) as session:
        healthcare_provider = session.query(HealthcareProvider).filter_by(idx=idx).first()
    engine.dispose()
    return healthcare_provider

def get_hpmatch(prompt, limval):
    engine = create_sql_engine()
    with Session(engine) as session:
        search = text("MATCH (name, street_address, medicare_provider_number, full_address) AGAINST (:prompt)"
            ).bindparams(prompt=prompt)
        retval = session.query(HealthcareProvider).filter(search).limit(limval)
    engine.dispose()
    return retval
    
class TestQueries(unittest.TestCase):
    def test_all_counties(self):
        counties = get_all_counties()
        self.assertEqual(len(counties), 260)

    def test_county(self):
        county_1 = get_specific_county(1)
        self.assertEqual(county_1, None)

        county_2 = get_specific_county(7)
        self.assertEqual(county_2.county, "Columbia")

    def test_search_county(self):
        county = get_cmatch("Columbia", 10)

    def test_all_coal_mines(self):
        coal_mines = get_all_coal_mines()
        self.assertEqual(len(coal_mines), 998)

    def test_coal_mine(self):
        coal_mine_1 = get_specific_coal_mine(1)
        self.assertEqual(coal_mine_1.mineName, "Black Warrior Mine No 1")

        coal_mine_2 = get_specific_coal_mine(-1)
        self.assertEqual(coal_mine_2, None)

    def test_search_coalmine(self):
        coal_mine = get_cmmatch("Black Warrior Mine No 1", 10)

    def test_all_healthcare_providers(self):
        healthcare_providers = get_all_healthcare_providers()
        self.assertEqual(len(healthcare_providers), 306)

    def test_healthcare_provider(self):
        healthcare_provider_1 = get_specific_healthcare_provider(1)
        self.assertEqual(healthcare_provider_1.name, "St Vincents East")

        healthcare_provider_2 = get_specific_coal_mine(-1)
        self.assertEqual(healthcare_provider_2, None)
    
    def test_search_hp(self):
        healthcare_provider = get_hpmatch("St Vincents East", 10)

if __name__ == "__main__":
    unittest.main()
    # counties = get_all_counties()
    # print([c.state for c in counties])
