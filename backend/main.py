import os
import subprocess
# The big boi API
from flask import *
from flask_cors import CORS
from query_db import *
import math
import re
from ast import literal_eval
from bs4 import BeautifulSoup
from whoosh.fields import Schema, TEXT
from whoosh.index import create_in
from whoosh.qparser import MultifieldParser
from whoosh.filedb.filestore import RamStorage


app = Flask(__name__)
CORS(app)
instances_per_page = 10

@app.route("/api")
def homepage():
    return "API is up!"

@app.route("/api/counties")
@app.route("/api/counties/<pagenum>")
def get_counties(pagenum=None):
    db_counties = [{
        "idx" : i.idx,
        "median_income" : i.median_income,
        "population" : i.population,
        "state" : i.state,
        "county" : i.county,
        "fips" : i.fips,
        "close_coal_mines" : literal_eval(i.close_coal_mines),
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        } for i in get_all_counties(list(request.args.items()))]
    db_counties = sort_by(request.args, db_counties)
    db_counties = apply_search(request.args, db_counties)

    if pagenum is None:
        return db_counties
    pagenum = int(pagenum) - 1
    if len(db_counties) == 0:
        return [0, []]
    num_pages = math.ceil(len(db_counties) / instances_per_page)
    # Wrap page around if page > num_pages
    start_idx = ((int(pagenum) - 1) % num_pages) * instances_per_page
    return [num_pages, *db_counties[start_idx : start_idx + instances_per_page]]

@app.route("/api/county/<idx>")
def county(idx):
    i = get_specific_county(idx)
    if i is None:
        return {}
    
    retval = {
        "idx" : i.idx,
        "median_income" : i.median_income,
        "population" : i.population,
        "state" : i.state,
        "county" : i.county,
        "fips" : i.fips,
        "close_coal_mines" : literal_eval(i.close_coal_mines),
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        }
    
    return retval

@app.route("/api/counties/search/<string:prompt>")
def search_county(prompt):
    db_counties = [{
        "idx" : i.idx,
        "median_income" : i.median_income,
        "population" : i.population,
        "state" : i.state,
        "match_str" : find_matches([i.county, i.fips, i.state], prompt),
        "county" : i.county,
        "fips" : i.fips,
        "close_coal_mines" : literal_eval(i.close_coal_mines),
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        } for i in get_cmatch(prompt, instances_per_page>>1)]

    return db_counties

@app.route("/api/coal_mines")
@app.route("/api/coal_mines/<pagenum>")
def get_coal_mines(pagenum=None):
    db_coal_mines = [{
        "idx" : i.idx,
        "mineName" : i.mineName,
        "coalRankDescription" : i.coalRankDescription,
        "average_employees" : i.average_employees,
        "mineStatusDescription" : i.mineStatusDescription,
        "production" : i.production,
        "latitude" : i.latitude,
        "longitude" : i.longitude,
        "close_county" : i.close_county,
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        } for i in get_all_coal_mines(list(request.args.items()))]
    db_coal_mines = sort_by(request.args, db_coal_mines)
    db_coal_mines = apply_search(request.args, db_coal_mines)

    if pagenum is None:
        return db_coal_mines
    pagenum = int(pagenum) - 1
    if len(db_coal_mines) == 0:
        return [0, []]
    num_pages = math.ceil(len(db_coal_mines) / instances_per_page)
    # Wrap page around if page > num_pages
    start_idx = (int(pagenum) % num_pages) * instances_per_page
    return [num_pages, *db_coal_mines[start_idx : start_idx + instances_per_page]]

@app.route("/api/coal_mine/<idx>")
def coal_mine(idx):
    i = get_specific_coal_mine(idx)
    if i is None:
        return {}
    
    retval = {
        "idx" : i.idx,
        "mineName" : i.mineName,
        "coalRankDescription" : i.coalRankDescription,
        "average_employees" : i.average_employees,
        "mineStatusDescription" : i.mineStatusDescription,
        "production" : i.production,
        "latitude" : i.latitude,
        "longitude" : i.longitude,
        "close_county" : i.close_county,
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        }
    
    return retval

@app.route("/api/coal_mines/search/<string:prompt>")
def search_coalmines(prompt):
   db_coal_mines = [{
        "idx" : i.idx,
        "mineName" : i.mineName,
        "coalRankDescription" : i.coalRankDescription,
        "average_employees" : i.average_employees,
        "mineStatusDescription" : i.mineStatusDescription,
        "production" : i.production,
        "latitude" : i.latitude,
        "longitude" : i.longitude,
        "match_str" : find_matches([i.mineName, i.coalRankDescription, i.mineStatusDescription], prompt),
        "close_county" : i.close_county,
        "close_healthcare_providers" : literal_eval(i.close_healthcare_providers)
        } for i in get_cmmatch(prompt, instances_per_page>>1)]
   
   return db_coal_mines



@app.route("/api/healthcare_providers")
@app.route("/api/healthcare_providers/<pagenum>")
def get_healthcare_providers(pagenum=None):
    db_healthcare_providers = [{
        "idx" : i.idx,
        "name" : i.name,
        "street_address" : i.street_address,
        "hospital_bed_count" : i.hospital_bed_count,
        "chrch_affl_f" : i.chrch_affl_f,
        "medicare_provider_number" : i.medicare_provider_number,
        "full_address" : i.full_address,
        "hospital_id" : i.hospital_id,
        "close_county" : i.close_county,
        "close_coal_mines" : literal_eval(i.close_coal_mines)
        } for i in get_all_healthcare_providers(list(request.args.items()))]
    db_healthcare_providers = sort_by(request.args, db_healthcare_providers)
    db_healthcare_providers = apply_search(request.args, db_healthcare_providers)

    if pagenum is None:
        return db_healthcare_providers
    pagenum = int(pagenum) - 1
    if len(db_healthcare_providers) == 0:
        return [0, []]
    num_pages = math.ceil(len(db_healthcare_providers) / instances_per_page)
    # Wrap page around if page > num_pages
    start_idx = (int(pagenum) % num_pages) * instances_per_page
    return [num_pages, *db_healthcare_providers[start_idx : start_idx + instances_per_page]]

@app.route("/api/healthcare_provider/<idx>")
def healthcare_provider(idx):
    i = get_specific_healthcare_provider(idx)
    if i is None:
        return {}
    
    retval = {
        "idx" : i.idx,
        "name" : i.name,
        "street_address" : i.street_address,
        "hospital_bed_count" : i.hospital_bed_count,
        "chrch_affl_f" : i.chrch_affl_f,
        "medicare_provider_number" : i.medicare_provider_number,
        "full_address" : i.full_address,
        "hospital_id" : i.hospital_id,
        "close_county" : i.close_county,
        "close_coal_mines" : literal_eval(i.close_coal_mines)
        }
    
    return retval

@app.route("/api/healthcare_providers/search/<string:prompt>")
def search_healthcare_providers(prompt):
    db_healthcare_providers = [{
        "idx" : i.idx,
        "name" : i.name,
        "street_address" : i.street_address,
        "hospital_bed_count" : i.hospital_bed_count,
        "chrch_affl_f" : i.chrch_affl_f,
        "medicare_provider_number" : i.medicare_provider_number,
        "full_address" : i.full_address,
        "hospital_id" : i.hospital_id,
        "match_str" : find_matches([i.name, i.street_address, i.medicare_provider_number, i.full_address], prompt),
        "close_county" : i.close_county,
        "close_coal_mines" : literal_eval(i.close_coal_mines)
        } for i in get_hpmatch(prompt, instances_per_page>>1)]
    return db_healthcare_providers

@app.route("/api/search/<string:prompt>")
def fullsite_search(prompt):
    #model card searches
    results = [{"models" : search_healthcare_providers(prompt) + search_coalmines(prompt) + search_county(prompt)}]

    home = 'https://www.coal-reality.me/'
    about = 'https://www.coal-reality.me/about'

    links = {"home" : home, "about" : about}


    schema = Schema(title=TEXT(stored=True), content=TEXT(stored=True))

    storage = RamStorage()
    index = storage.create_index(schema)
    storage.open_index()

    writer = index.writer()

    for link in links:
        curl_cmd = ['/usr/bin/curl', links[link]]
        try:
            result = subprocess.run(args=curl_cmd, check=True, text=True, capture_output=True, universal_newlines=True)

            if(result.returncode == 0):
                # Parse HTML
                soup = BeautifulSoup(result.stdout, 'html.parser')

                writer.add_document(title=link, content=soup.get_text())
            else:
                return results

        except subprocess.CalledProcessError as e:
            return results

    writer.commit()

    searcher = index.searcher()

    parser = MultifieldParser(["title", "content"], schema=index.schema)

    query = parser.parse(prompt)

    searched = searcher.search(query)

    for i in searched:
        title_highlight = i.highlights("title")
        content_highlight = i.highlights("content")
        results.append({"pages" : {"title" : i["title"], "str_match" : [element.get_text() for element in BeautifulSoup(title_highlight, 'html.parser').find_all('b', class_=re.compile(r'match term\d+'))] + [element.get_text() for element in BeautifulSoup(content_highlight, 'html.parser').find_all('b', class_=re.compile(r'match term\d+'))]

}})
    
    searcher.close()

    return results


@app.route("/api/test_counties")
def test_county_api():
    return list(request.args.items())

if __name__ == "__main__":
    app.run(debug=True, host = os.environ.get("HOST", "0.0.0.0"))