**Canvas / Ed Discussion group number:**

Group 3

**Website Link**
 https://coal-reality.me/

**Backend Information**

Backend API Link (nothing exists at this root, use endpoints under api: ex. www.api.coal-reality.me/api/test_counties): www.api.coal-reality.me/api

API Docs Link: https://documenter.getpostman.com/view/30659555/2s9YRGxUhN

**Git SHA**
| Phase #  | SHA|
| ---------| ---|
| Phase I  | 887d5bc3d258abca68b3685c941791cb2665def5 |
| Phase II | 57148d746a0309873dab0b13b5f91afe081a3b82 | 
| Phase III| 41db81bb1728278297d6b890b1c5a03a4c803a02 |
| Phase IV | 99b5fa37838540ce12dd2dc1105aa7be5306b20b |

**Team Leader Phase 1**
|Phase #| Leader|
|-------| ------|
|1| Yash Saxena |
|2| Jeffrey Jiang|
|3| Emily Yue |
|4| Charlotte Stinson |

Responsibilities of Phase Leader: To oversee the progress of the group as a whole and check up on the individual sub-teams progress.

**names of the team members:**

Aribah Hoque, Jeffrey Jiang, Yash Saxena, Charlotte Stinson, Emily Yue

Aribah Hoque
Gitlab ID: aribah911
Commits: 25
Issues: 25
Unit Tests: 4

Yash Saxena
Gitlab ID: saxenaya
Commits: 46
Issues: 14
Unit Tests: 3

Jeffrey Jiang
Gitlab ID: AnonymousAmalgrams
Commits: 98
Issues: 8
Unit Tests: 3

Charlotte Stinson
Gitlab ID: charlottestinson
Commits: 48
Issues: 25
Unit Tests: 16

Emily Yue
Gitlab ID: emily.yue
Commits: 61
Issues: 24
Unit Tests: 10

**Hours Worked**
| Phase #| Estimated | Actual| 
| -------| ----------| ------| 
|1       | 30        |  35   |
|2       | 30        |  40   |
|3       | 35        |  35   |
|4       | 35        |  35   |

**name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL)**

Coal-Reality

**the proposed project**

Coal mining has historically been a major source of jobs and economic stability in the Appalachian region. This has unfortunately also led to a lot of environmental issues that cause health risks for those living in the region. Our website hopes to provide information about coal mines in the Appalachian region and nearby healthcare providers that can provide support for those who have been affected by health issues from coal mining.

**URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this)**

https://www.arcgis.com/home/item.html?id=8f616443c11b455ebf6001721c563a46

https://data.lib.vt.edu/articles/dataset/Appalachian_Coal_Data/14098733/1?file=26587091 

https://www.eia.gov/coal/data/browser/#/topic/38?agg=0,2,1&rank=g&geo=00000000g&mntp=g&freq=A&start=2001&end=2021&ctype=map&ltype=pin&rtype=s&maptype=0&rse=0&pin=&datecode=2021

https://geopub.epa.gov/dwwidgetapp/

https://data.cms.gov/provider-data/dataset/eedd-4c6c

**at least three models**

Counties

Coal Mines

Healthcare Providers

**an estimate of the number of instances of each model**

Counties: 423

Coal Mines: 200

Healthcare Providers: 500

**each model must have many attributes
describe five of those attributes for each model that you can filter or sort**

Counties:

- Population

- State

- Avg Age

- Avg Income

- Employment Rates

- Coal Mines

- Healthcare Providers

Coal Mines

- County

- Status (Inactive/Active)

- Mining Method

- Production

- Basin

Healthcare Providers

- Name

- County 

- Physical Address

- Telephone Number

- Specialty Services

- Insurance Accepted

**instances of each model must connect to instances of at least two other models**

**instances of each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this)**

**describe two types of media for instances of each model**

Counties: maps and images

Coal Mines: map of location, images of mines

Healthcare Providers: text about provider information, images of building, map of location


**describe three questions that your site will answer**

What mines are in the Appalachian region?

If I live near a mine and have been affected by health issues because of it, what healthcare providers are near me?

What counties are in the Appalachian region that are affected by the negative health and environmental effects of coal mines?


What diseases are supported by a specific organization in the Appalacian region?
