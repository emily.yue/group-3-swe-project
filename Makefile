.DEFAULT_GOAL := all
SHELL         :=  bash

# get git config
config:
	git config -l

# get git log
coalreality.log.txt:
	git log > coalreality.log.txt

# get git status
status:
	make --no-print-directory clean
	@echo
	git branch
	git remote -v
	git status

# download files from the coalreality code repo
pull:
	make --no-print-directory clean
	@echo
	git pull
	git status

# upload files to the coalreality code repo
push:
	make --no-print-directory clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Makefile
	git add README.md
	git commit -m "another commit"
	git push
	git status

test:
	cd coal-reality && npm run dev
	# echo $! > frontend.pid

build: 
	cd coal-reality && npm run build
	# echo $! > testrun.pid

# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml

# check the existence of check files
check: $(C_FILES)

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

#run frontend selenium tests
selenium-tests:
	@echo "Running Selenium unit tests..."
	@python3 coal-reality/tests/guitests.py

# run jest tests
# jest-tests:
# 	@echo "Running Jest unit tests..."
# 	@npm test