module.exports = {
transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.(ts|tsx)$': 'ts-jest',
},
moduleNameMapper: {
    '\\.(css)$': 'jest-css-modules',
},
testEnvironment: 'jsdom',
};
