import Gitlab from "./image/gitlab.png";
import Postman from "./image/postman.png";
import VSCode from "./image/vscode.png";
import React from "./image/react.png";
import Bootstrap from "./image/bootstrap.png";
import NameCheap from "./image/namecheap.png";
import AWS from "./image/aws.png";

const toolInfo = [
    {
        image: "https://www.svgrepo.com/show/349377/gitlab.svg",
        name: "GitLab",
        description: "GitLab is a web-based platform for version control and collaborative software development, offering features like code repository management, issue tracking, and CI/CD automation."
    },
    {
        image: "https://seeklogo.com/images/P/postman-logo-0087CA0D15-seeklogo.com.png",
        name: "Postman",
        description: "Postman is a versatile API development and testing tool that streamlines the creation, testing, and documentation of APIs through an intuitive interface, making it a valuable resource for developers and teams working with APIs."
    },
    {
        image: "https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg",
        name: "Visual Studio Code",
        description: "Visual Studio Code (VS Code) is a lightweight and highly customizable source code editor developed by Microsoft. It's widely used by developers for its robust features, extensions, and support for various programming languages, making it a favored choice for software development."
    },
    {
        image: "https://cdn1.iconfinder.com/data/icons/programing-development-8/24/react_logo-1024.png",
        name: "React",
        description: "React is an open-source JavaScript library used for building user interfaces for web and mobile applications. Developed and maintained by Facebook, React enables developers to create interactive, reusable UI components and efficiently manage the state of their applications, resulting in faster and more maintainable front-end development."
    },
    {
        image: "https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg",
        name: "Bootstrap",
        description: "Bootstrap is an open-source front-end framework developed by Twitter that simplifies web development by providing a collection of pre-designed, responsive CSS and JavaScript components. It allows developers to quickly create visually appealing and mobile-friendly websites and web applications by utilizing its ready-made design elements, grid system, and UI components."
    },
    {
        image: "https://www.svgrepo.com/show/354100/namecheap.svg",
        name: "NameCheap",
        description: "Namecheap is a popular domain registrar and web hosting provider known for its user-friendly services and competitive pricing."
    },
    {
        image: "https://upload.wikimedia.org/wikipedia/commons/9/93/Amazon_Web_Services_Logo.svg",
        name: "Amazon Web Services",
        description: "AWS (Amazon Web Services) is a leading cloud computing platform by Amazon, offering a broad array of cloud services for scalable and flexible application development and management."
    }
]

export { toolInfo };