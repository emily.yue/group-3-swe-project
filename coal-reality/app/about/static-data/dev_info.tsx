const DevInfo = [
    {
        image: "https://miro.medium.com/v2/resize:fit:1358/1*ElkDR0f11H2zky016WbD_w.jpeg",
        name: "Aribah Hoque",
        git_user: "aribah911",
        role: "Frontend Developer",
        bio: "I am a junior at UT Austin pursuing a Computer Science degree. I enjoy working out, traveling, and spending time with my friends and family.",
        email: "aribah.hoque@icloud.com",
        alt_email : "70315405+aribah911@users.noreply.github.com",
        commits: 0,
        issues: 0,
        tests: 4
    },

    {
        image: "https://miro.medium.com/v2/resize:fit:636/format:webp/0*A2HcYgSK3FoaBIyP.jpeg",
        name: "Yash Saxena",
        git_user: "saxenaya",
        role: "Backend Developer",
        bio: "I'm a junior at UT Austin pursuing a BS in Computer Science. My interests include running, rock climbing, playing board games, and cooking for friends and family.",
        email: "yash.saxena@utexas.edu",
        alt_email : "yash.saxena@gmail.com",
        commits: 0,
        issues: 0,
        tests: 3
    },

    {
    image: "https://cs373spring2023com.files.wordpress.com/2023/09/unnamed-1.jpg",
    name: "Jeffrey Jiang",
    git_user: "AnonymousAmalgrams",
    role: "Backend Developer",
    bio: "I'm a junior at UT Austin pursuing a BS in Computer Science with a Quantum Information Science certificate and a minor in Economics. My interests include strategy and gacha games, reading, military history, and finance.",
    email: "jeffreyj2314@gmail.com",
    commits: 0,
    issues: 0,
    tests: 3
    },

    {
        image: 'https://media.licdn.com/dms/image/D5603AQGXgSwM0D0GRw/profile-displayphoto-shrink_800_800/0/1683306572227?e=2147483647&v=beta&t=_7s9nl9CodZulq89Qsl0Cdrud74IkTL9N2nUHSiCi8Y',
        name: "Charlotte Stinson",
        git_user: "charlottestinson",
        role: "Frontend Developer",
        bio: "I'm a junior at UT pursuing degrees in Computer Science and Plan II Honors. I enjoy baking, playing piano, and going to UT football games.",
        email: "charlottestinson@utexas.edu",
        commits: 0,
        issues: 0,
        tests: 16
    }, 

    {
        image: 'https://miro.medium.com/v2/resize:fit:1100/format:webp/1*Eml-YuN0Z20ZomJJo2dfXA.jpeg',
        name: " Emily Yue",
        git_user: "emily.yue",
        role: "Frontend Developer",
        bio: "I'm a senior at UT Austin pursuing a degree in Computer Science. In my free time I like playing board games, reading, and taking walks.",
        email: "emilyyue08@gmail.com",
        commits: 0,
        issues: 0,
        tests: 10
    }
    
];

export { DevInfo };
