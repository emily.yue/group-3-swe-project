const apiInfo = [
    {
        image: "https://www.svgrepo.com/show/349377/gitlab.svg",
        name: "GitLab",
        description: "GitLab is a web-based platform for version control and collaborative software development, offering features like code repository management, issue tracking, and CI/CD automation."
    }
];

export { apiInfo };