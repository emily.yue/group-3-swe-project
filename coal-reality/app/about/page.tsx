'use client'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { DevInfo } from "./static-data/dev_info";
import { apiInfo } from './static-data/api_info';
import { toolInfo } from './static-data/tool_info';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const accessToken = 'glpat-6csD8yYxLvNFhvcQmbvx';

const userData = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
    'Authorization' : 'Bearer' + accessToken
  },
  baseURL: "https://gitlab.com/api/v4/projects/50469433"
});

function About() {

  const[totalCommits, updateTotalCommits] = useState(0);

  useEffect(() => {
    let allIssues = 0;
      DevInfo.forEach((user) => {
        userData.get("issues_statistics?author_username=" + user.git_user).then((response) => {
          user.issues = response.data['statistics']['counts']['closed'];
        })
      })

    let allCommits = 0;
    userData.get("repository/contributors").then((response) => {
      response.data.forEach((info: { name: any; email: any; commits: any; }) => {
        const {name, email, commits } = info;
        DevInfo.forEach((user) => {
          if (user.name === name || user.email === email || user.git_user === name || user.alt_email === email) {
            user.commits = commits;
            allCommits += commits;
          }
        });
      });
      updateTotalCommits(allCommits);

    });

  }, []);
  return (
    <Container>
    
      <div>
        <h1 className='text-center mt-4'>About Appalachia&apos;s Coal Reality</h1>
      </div>
      <br></br>
      <div>
        <p>Coal mining has historically been a major source of jobs and economic stability in the Appalachian region. This has unfortunately also led to a lot of environmental issues that cause health risks for those living in the region.</p>
        <p>Our website hopes to provide information about coal mines in the Appalachian region and nearby healthcare providers that can provide support for those who have been affected by health issues from coal mining.</p>
        <p>With our models, users can check if there are coal mines in their vicinity and have access to nearby healthcare providers in order to get the support they need without the need to look for support.</p>
      </div>
      <br></br>
      <div>
        <h1 className='text-center'>Tools</h1>
      </div>
      <div>
        <Row>
          {
            toolInfo.map((tools) => {
              return (
                <Col key = {tools.name} md="3">
                                <Card key = {tools.name}
                                    style={{ width: '20rem', height: '40rem', marginBottom: '30px' }}>
                                    <Card.Img variant="top" style = {{ width: '20rem', height: '19rem'}} src={tools.image} />
                                    <Card.Body>
                                        <Card.Title>{tools.name}</Card.Title>
                                        <Card.Text>{tools.description}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
              )
            })
          }
        </Row>
      </div>
      <br></br>
      <div>
        <h1 className='text-center'>API</h1>
      </div>
      <div>
        <Row>
          {
            apiInfo.map((api) => {
              return (
                <Col key = {api.name} md="auto">
                                <Card key = {api.name}
                                    style={{ width: '20rem', height: '50rem' }}>
                                    <Card.Img variant="top" style = {{ width: '20rem', height: '19rem' }} src={api.image} />
                                    <Card.Body>
                                        <Card.Title>{api.name}</Card.Title>
                                        <Card.Text>{api.description}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
              )
            })
          }
        </Row>
      </div>
      <br></br>
      <div>
        <h1 className='text-center'>Our Team</h1>
      </div>
      <div>
      <Row>
                {
                    DevInfo.map((member) => {
                        return (
                            <Col key = {member.name} md="3">
                                <Card key = {member.name}
                                    style={{ width: '20rem', height: '50rem', marginBottom: '10px' }}>
                                    <Card.Img variant="top" style = {{ width: '20rem', height: '19rem' }} src={member.image} />
                                    <Card.Body>
                                        <Card.Title>{member.name}</Card.Title>
                                        <Card.Subtitle>{"Role: " + member.role}</Card.Subtitle>
                                        <Card.Text>{"Gitlab ID: " + member.git_user}</Card.Text>
                                        <Card.Text>{"About me: " + member.bio}</Card.Text>
                                        <Card.Text>{"Commits: " + member.commits}</Card.Text>
                                        <Card.Text>{"Issues: " + member.issues}</Card.Text>
                                        <Card.Text>{"Unit Tests: " + member.tests}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })
                }
            </Row>

      </div>
      <br></br>
      <div>
        <h1 className='text-center'>Total GitLab Statistics</h1>
      </div>
      <div className="text-center">
        <Card>
          <Card.Body>
            <Card.Subtitle as="h5">Total Commits: {totalCommits}</Card.Subtitle>
            <br></br>
            <Card.Subtitle as="h5">Total Issues: {DevInfo[0].issues + DevInfo[1].issues + DevInfo[2].issues + DevInfo[3].issues + DevInfo[4].issues}</Card.Subtitle>
          </Card.Body>
        </Card>
      </div>
      <br></br>
      <div className="text-center mb-3">
        <a href="https://documenter.getpostman.com/view/30659555/2s9YRGxUhN">
          <Button variant="dark">Our API</Button>
        </a>
      </div>
      <div className="text-center mb-3">
        <a href="https://gitlab.com/emily.yue/group-3-swe-project">
          <Button variant="dark">Our Repository</Button>
        </a>
      </div>


    </Container>
  );
}

export default About