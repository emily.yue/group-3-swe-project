import axios from "axios";

const client = axios.create( {
    headers: {
      'Content-Type' : 'application/json',
    },
    baseURL: "https://api.coal-reality.me/api"
    // baseURL:  "http://127.0.0.1:5000/api/"
  });

export const searchWebsite = async (search_text : any) => {
    const response = await client.get('/search/' + search_text);
    return response.data;
};

export const search_model = async (searchtext : any, model : string) => {
    // const response = await client.get('/' + model + '?search=' + searchtext);
    const response = await client.get('/' + model + '/search/' + searchtext);
    return response.data;
};