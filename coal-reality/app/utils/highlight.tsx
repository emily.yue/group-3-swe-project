import Highlighter from 'react-highlight-words';

export function highlightText(text : any, query : any) {
    if(query === '') {
        return text;
    }
    if (typeof query === 'string') {
        const words = query.split(/\s+/);
        return (
            <Highlighter 
                highlightStyle={{
                    padding: '0',
                    backgroundColor: '#ffd54f',
                }}
                searchWords={words}
                textToHighlight={text.toString()}
                autoEscape={true}
            />
        );
    }
    return text;
}