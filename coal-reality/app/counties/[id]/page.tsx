'use client'
import 'bootstrap/dist/css/bootstrap.min.css';  
import { Container } from "@chakra-ui/react";
import CountiesInstance from '../CountiesInstance'
import React, {useEffect, useState } from 'react'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'
import axios from 'axios'

// change to real data
import  counties_img from '../../images/county_img.json'
//import CoalMinesInfo from '../../../../backend/coal_mine_data.json'
//import HealthcareProvidersInfo from '../../../../backend/healthcare_providers.json';


// Function to find the URL by name
function findUrlByName(name: string): string {
    const entry = counties_img.find(item => item.name === name);
    if (entry == null) {
        return ""
    }
    return entry.url;
}


const getCoalMines = (close_cloal_mines: number[], all_coal_mines : any[]) => {
    let coal_mines : any[] = []
    close_cloal_mines.forEach((coal_mine_idx) => {
      let coal_mine = all_coal_mines.find((coal_mine) => coal_mine.idx === coal_mine_idx)
      if (coal_mine !== undefined) {
        coal_mines.push(coal_mine);
      }
    })
    return coal_mines;
}

const getHealthcareProviders = (close_healthcare_providers: number[], all_healthcare_providers : any[]) => {
    let healthcare_providers: any[] = [];
    close_healthcare_providers.forEach((healthcare_provider_idx) => {
        let healthcare_provider = all_healthcare_providers.find((healthcare_provider) => healthcare_provider.idx === healthcare_provider_idx)
        if (healthcare_provider !== undefined) {
            healthcare_providers.push(healthcare_provider);
        }
    })
    return healthcare_providers;
}


const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  baseURL: "https://api.coal-reality.me/api"
});

function CountyPage({params}: {params: { id: string }}) {
    // const county = CountiesInfo.find((county) => county.idx === parseInt(params.id));    

    //const countyMapLink = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyDSFN1kt4StJE_0zKTdX8uC9-T8489W-2w&q=' + county.County;
    const[county, setCounty] = useState<any>(null);
    const[coalMines, setCoalMines] = useState([]); // change later to [] after api is set up
    const[healthcareProviders, setHealthcareProviders] = useState([]); // change later to [] after api is set up 
    const[related_coal_mines, setRelatedCoalMines] = useState<any>([]);
    const[related_healthcare_providers, setRelatedHealthcareProviders] = useState<any>([]);

    // google maps API key: AIzaSyDSFN1kt4StJE_0zKTdX8uC9-T8489W-2w

    useEffect(() => {
        client.get("county/"+params.id.toString()).then((response) => {
            console.log(response.data);
            setCounty(response.data);     
        })
        client.get("coal_mines").then((response) => {
            setCoalMines(response.data);
        })
        client.get("healthcare_providers").then((response) => {
            setHealthcareProviders(response.data);
        })
    }, [])

    useEffect(() => {
        if(county !== undefined && county !== null) {
            setRelatedCoalMines(getCoalMines(county.close_coal_mines, coalMines));
            setRelatedHealthcareProviders(getHealthcareProviders(county.close_healthcare_providers, healthcareProviders));
        }
    }, [county, coalMines, healthcareProviders])

    if (county === null || coalMines.length == 0 || healthcareProviders.length == 0 || related_coal_mines == null) {
        // console.log("-------------------")
        // console.log(county)
        // console.log(coalMines)
        // console.log(healthcareProviders)
        // console.log(related_coal_mines)
        // console.log(related_coal_mines.idx)
        return (
            // <Container>
            //     <h1>County Not Found</h1>
            // </Container>
            <div></div>
        )
    }
    return (
        <Container>
            <CountiesInstance 
            title={county.county}
            prop1={county.population}
            prop2={county.state}
            prop3={county.median_income}
            prop4={related_coal_mines.map(function(related_coal_mine : any) {return related_coal_mine.mineName.toString();}).join(', ')}
            prop5={related_healthcare_providers.map(function(related_healthcare_provider : any) {return related_healthcare_provider.name.toString();}).join(', ')}
            imgSrc={findUrlByName(county.county + " county, " + county.state)}
            mapSrc={'https://www.google.com/maps/embed/v1/place?key=AIzaSyDSFN1kt4StJE_0zKTdX8uC9-T8489W-2w&q=' + county.county + ' County' + county.state}></CountiesInstance>

            <Row>
                <Col className='ms-4'>
                    <Card className="ms-auto me-auto mb-4 mt-4">
                            <Card.Header>Coal Mines:</Card.Header>
                            <Card.Body>
                                {related_coal_mines.map((coal_mine : any) => {
                                    return (
                                        <Link key={coal_mine.idx} href={'../coal-mines/' + coal_mine.idx.toString()}>
                                            <Button className='w-100 mb-4'>
                                                {coal_mine.mineName}
                                            </Button>
                                        </Link> 
                                    )
                                })}
                            </Card.Body>
                    </Card>
                </Col>
                <Col className="me-4">
                    <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>Healthcare Providers</Card.Header>
                        <Card.Body>
                            {related_healthcare_providers.map((healthcare_provider : any) => {
                                return (
                                    <Link key={healthcare_provider.idx} href={'../healthcare-providers/' + healthcare_provider.idx.toString()}>
                                        <Button className='w-100 mb-4'>
                                            {healthcare_provider.name}
                                        </Button>
                                    </Link> 
                                )
                            })} 
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container> 
    )
}
    
export default CountyPage;