'use client'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import CountiesCard from './CountiesCard'
import PaginationBar from '../components/paginationbar';
import FilterDropdown from '../components/filterdropdown';
import SearchBar from '../components/search-bar';
import { search_model } from '../utils/api';

// change later
import counties_img from '../images/county_img.json'


// Function to find the URL by name
function findUrlByName(name: string): string {
  const entry = counties_img.find(item => item.name === name);
  if (entry == null) {
      return ""
  }
  return entry.url;
}

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  // baseURL: "http://127.0.0.1:5000/api/"
 baseURL: "https://api.coal-reality.me/api" 
});

const cards_per_page = 10;

// filtering options
const possible_states = ["Alabama", "Virginia", "Pennsylvania", "South Carolina", "Georgia", "New York", "Ohio", "Maryland", "North Carolina", "West Virginia", "Kentucky", "Tennessee", "Mississippi"]
const sort_by_map: Record<string, string> = {
  'County Name': 'county',
  'Population': 'population',
  'Median Income ': 'median_income',
  'State': 'state'
}
const possible_hcare = ["Emory Johns Creek Hospital", "Memorial Medical Center", "Muhlenberg Community Hospital", "Forbes Regional Hospital", "Armstrong County Memorial", "Brookville Hospital", "Buchanan General Hospital", "Methodist Le Bonheur Germantown Hospital", "Jellico Community Hospital", "Grant Memorial Hospital", "Grady Memorial Hospital", "Our Lady Of Bellefonte Hospital", "St Vincents Birmingham", "Baptist Health Madisonville", "St Elizabeth - Grant County", "Methodist Hospital Union County", "Russell County Hospital", "Garden Park Community Hospital", "Carthage Area Hospital, Inc", "Shelby Baptist Medical Center", "Mercy Health - Fairfield Hospital", "Physicians Care Surgical Hospital", "Saint Joseph East", "The Washington Hospital", "Randolph Hospital", "Good Samaritan Hospital", "University Of Louisville Hospital", "Tug Valley Arh", "University Pointe Surgical Hospital", "Johnson County Community Hospital", "Blue Mountain Hospital", "University Of Tennessee Medical Cent", "Samaritan Medical Center", "Mid-Valley Hospital", "Franklin Woods Community Hospital", "Methodist H/C Fayette Hospt", "Upmc Jameson", "Shriners Hospitals For Children - C", "Summersville Regional Med Center", "Lehigh Valley Hospital Schuylkill Ea", "Laughlin Memorial Hospital, Inc", "Monongalia General Hospital", "Ahn The Medical Center", "Community Gen Osteopathic Hospital", "Miners Medical Center", "Cullman Regional", "Palmerton Hospital", "Barnesville Hospital", "Bedford Memorial Hospital", "Ellwood City Hospital", "Wayne Medical Center", "Tennova Healthcare Shelbyville", "Miners Memorial Medical Center", "Greene Memorial Hospital  Inc", "St Francis Hospital Of New Castle", "Dickenson Community Hospital", "Upmc Childrens Hospital Of Pgh", "Dubois Regional Medical Center", "Eagleville Hospital", "Baptist Health Lexington", "James B Haggin Memorial Hospital", "Shriners Hospitals For Children", "St Joseph Medical Center", "River Hospital", "Columbia Memorial Hospital", "Firsthealth Montgomery Memorial Cah", "Upmc Cranberry", "Our Lady Of The Way", "Marian Community Hospital", "Fort Hamilton Hospital", "Wilson Memorial Hospital", "Jackson General Hospital", "Whitesburg Arh", "Northwest Medical Center", "Suburban Hospital", "Piedmont Fayette Hospital Inc", "Union General Hospital", "Mccready Memorial Hospital", "Dayton Childrens Hospital", "St Joseph Hospital", "St Marys Healthcare", "Pineville Community Health Center", "Lehigh Valley Hospital-Hazleton", "Upmc Magee-Womens Hospital", "Nicholas County Hospital", "Alliance Community Hospital", "Butler County Medical Center", "Ahn Wexford Hospital", "Atlanta Medical Center", "Owensboro Health Regional Hospital", "Community Medical Center", "Moses Taylor Hospital", "Carilion Tazewell Community Hospital", "Kings Daughtersmedical Center", "Mary Breckinridge Hospital", "Upmc St Margaret", "The Childrens Institute", "West Virginia University Hospitals", "Baptist Memorial Hospital-Collierville", "Cuba Memorial Hospital Inc", "Butler Memorial Hospital", "Potomac Valley Hospital", "West Chester Hospital  Llc", "Greeneville Community Hospital", "United Hospital Center", "Lehigh Valley Hospital - Schuylkill", "Sunbury Community Hospital", "Nicholas H Noyes Memorial Hospital", "East Ohio Regional Hospital", "Highlands Hospital", "Tyler Memorial Hospital", "Wheeling Hospital", "Jefferson Memorial Hospital", "Mary Rutan Hospital", "Meritus Medical Center", "Adventhealth Manchester", "Shamokin Area Community Hospital", "Beacham Memorial Hospital", "St Francis Hospital", "Harlan Arh", "St Elizabeth Florence", "Webster Health Services", "Fulton County Health Center", "Frick Hospital", "Le Bonheur Childrens Hospital", "Regional One Health", "St Marys Medical Center Of Campbell County", "Allegheny General Hospital", "Nathan Littauer Hospital & Nursing H", "Upmc - Presbyterian Shadyside", "Broaddus Hospital Association", "Baptist Health Louisville", "Harrison Memorial Hospital", "Pike Community Hospital", "Clarion Hospital", "Suburban Community Hospital", "Uofl Health - Louisville", "Westmoreland Regional  Hospital", "Canonsburg General Hospital", "Middlesboro Arh", "Williamsport Hospital & Medical Ctr", "Jefferson Regional Medical Center", "Methodist H/C Memphis Hospt", "Newark Wayne Community Hospital", "Upmc Northwest Hospital", "Lee Regional Medical Center", "Coshocton Regional Medical Center", "Northridge Medical Center", "Upmc Shadyside", "Norton Audubon Hospital", "Holy Redeemer Hospital", "Baptist Hospital West", "Saint Joseph Mount Sterling", "Brookwood Baptist Medical Center", "St Marys Jefferson Memorial Hospital", "Lonesome Pine Hospital", "Thomas Memorial Hospital", "Uniontown Hospital", "Camc Women And Childrens Hospital", "Morgan Co Arh", "East Tennessee Childrens Hospital", "Marion Regional Medical Center", "Floyd Medical Center", "Pottstown Memorial Medical Center", "St Jude Childrens Hosp", "Reynolds Memorial Hospital", "Davis Memorial Hospital", "Upmc East Hospital", "Baptist Mem Hospital Union County", "Mercy Medical Center", "Barbourville Arh Hospital", "Wetzel County Hospital", "The Childrens Hospital Of Alabama", "Evangelical Community Hospital", "Muncy Valley Hospital", "Knox Community Hospital", "Punxsutawney Area Hospital", "Sharon Regional Health System", "Grandview Hospital", "Baptist Health Corbin", "North Mississippi Medical Center", "Union Hospital Association", "St Marys Good Samaritan", "Ohio County Hospital", "Soin Medical Center", "Upmc Mercy Hospital", "Carilion New River Valley Med Center", "Pikeville Medical Center", "Norton Suburban Hospital", "Montgomery Hospital", "Windber Hospital  Inc", "Kettering Memorial Hospital", "Hazard Arh", "Penn Highlands Elk", "Boone Memorial Hospital", "Upmc Lee Regional", "St Josephs Hospital Of Buckhannon", "Holy Cross Germantown Hospital", "Mercy Jeannette Hospital", "Lansdale Hospital", "Heritage Valley Kennedy", "Aultman Orrville Hospital", "Reading Hospital And Medical Center", "Wayne Memorial Hospital", "Charleston Area Medical Center  Inc", "Meyersdale Medical Center", "Northside Hospital", "Geisinger Stlukes", "Piedmont Hospital Inc", "Jersey Shore Hospital", "Webster County Memorial Hospital", "Jewish Hospital Shelbyville", "Norton Hospitals  Inc", "Harrison Community Hospital", "Upmc Bedford Memorial Hospital", "Clay County Medical Corporation", "Choctaw General Hospital", "Upmc Passavant", "Lankenau Medical Center", "St Josephs Of Atlanta", "Upmc Horizon Hospital", "Tanner Medical Center Alabama  Inc", "Baptist Mem Hospital Memphis", "Jackson Hospital And Clinic  Inc", "Doctors Hospital Of Nelsonville", "The Childrens Home Of Pittsburgh", "Obleness Memorial Hospital", "Regional Hospital Of Scranton", "Kings Daughters Medical Center", "Twin City Hospital", "Cah 1 - Washington", "Montgomery General Hospital", "Scottish Rite Childrens Medical Ctr", "War Memorial Hospital", "Anmed Health", "Geisinger Wyoming Valley Med Ctr", "Baptist Memorial Hospital For Women", "Mcdowell Arh", "Marshall County Hospital", "Livingston Hospital", "Parkwest Medical Center", "Alle-Kiski Medical Center", "Fairmont Regional Medical Center", "Carthage Area Hospital", "Emory University Hospital Midtown", "Pineville Community Hospital", "Saint Joseph London", "Mccullough-Hyde Memorial Hospital", "Somerset Hospital", "Adventhealth Redmond", "St Elizabeth Ft Thomas", "Grove City Medical Center", "Princeton Baptist Medical Center", "Heritage Valley Sewickley", "Selby General Hospital", "Indiana Regional Medical Center", "Latrobe Area Hospital", "Geisinger Bloomsburg Hospital", "Wayne County Hospital", "Fort Logan Hospital", "Monongahela Valley Hospital  Inc", "Tanner Medical Center-East Alabama", "Beckley Arh", "Medwest Harris", "Ccf Mercy Medical Center", "Upmc Pinnacle Hospitals", "Western Pennsylvania Hospital", "North Fulton Regional Hospital", "Sycamore Hospital", "Upmc-Western Maryland Corp", "St Clair Memorial Hospital", "Methodist Medical Center", "Callahan Eye Foundation Hosp", "Johnston Memorial Hospital", "Trinity Hospital Holding Company", "Marion General Hospital", "Fort Sanders Regional Medical Center", "Holzer Medical Center Jackson", "Ohio Valley General Hospital", "Highlands Regional Medical Center", "Miami Valley Hospital", "St Vincents East", "Belmont Community Hospital", "Ahc Shady Grove Medical Center", "Holy Cross Hospital", "Einstein Medical Center Montgomery", "Clearfield Hospital", "Jones Memorial Hospital", "Aultman Hospital", "Whs - Greene", "Saint Joseph Hospital", "Johnson City Medical Center", "Claiborne Medical Center", "Abington Memorial Hospital", "Medstar Montgomery Medical Center", "Marietta Memorial Hospital", "Sts Mary & Elizabeth Medical Center", "Fulton County Medical Center", "Walker Baptist Medical Center", "Upmc Mckeesport", "Grafton City Hospital"]

const Counties = () => {
  const[counties, setCounties] = useState<any[]>([]); // change later to [] after api is set up
  const[coalMines, setCoalMines] = useState([]); // change later to [] after api is set up
  const[healthcareProviders, setHealthcareProviders] = useState([]); // change later to [] after api is set up
  const[numCounties, setNumCounties] = useState(counties.length);
  const[currentPage, setCurrentPage] = useState(1);
  const[totalNumPages, setTotalNumPages] = useState(1);

  // filtering states
  const[stateFilter, setStateFilter] = useState("");
  const[sortByProperty, setSortByProperty] = useState("");
  const[hcareFilter, setHcareFilter] = useState("");

  const handleSearch = async (search_term : any) => {
    const search_data = await search_model(search_term, "counties");
    // this is data retrieved
    setCounties(search_data);
    setTotalNumPages(1);
  }
  
  useEffect(() => {
    client.get("coal_mines").then((response) => {
      setCoalMines(response.data);
    })
    client.get("healthcare_providers").then((response) => {
      setHealthcareProviders(response.data);
    })
  }, [])
  
  useEffect(() => {
    // client.get("counties/" + currentPage.toString()).then((response) => {
    //   setCounties(response.data.slice(1, 11));
    //   setTotalNumPages(response.data[0]); 
    //   // console.log(response.data.slice(1, 11));
    // })
    const stateURL = stateFilter === "" ? "" : "&state=" + stateFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by=" + sort_by_map[sortByProperty];
    
    const hcareMatch : any =  hcareFilter === "" ? "" : healthcareProviders.find((hcare : any) => {
      return hcare.name === hcareFilter;
    });
    let hcareURL = ""
    if (hcareMatch != undefined && hcareMatch != ""){
      hcareURL = "&close_healthcare_providers=" + hcareMatch.idx;
    }
    client.get("counties/" + currentPage.toString() + "?" + stateURL + sortByURL + hcareURL).then((response) => {
      setCounties(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
      // console.log(response.data.slice(1, 11));
    })
  }, [currentPage])

  useEffect(() => {
    setNumCounties(counties.length);
  }, [counties])

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }

  const nextPage = () => {  
    if (currentPage < totalNumPages) {
      setCurrentPage(currentPage + 1);
    }
  }

  const getCoalMineNames = (close_cloal_mines: number[]) => {
    if(close_cloal_mines == undefined){
      return [];
    }
    let coal_mine_names: string[] = [];
    close_cloal_mines.forEach((coal_mine_idx) => {
      let coal_mine : any = coalMines.find((coal_mine : any) => coal_mine.idx === coal_mine_idx)
      if (coal_mine !== undefined) {
        coal_mine_names.push(coal_mine.mineName.toString());
      }
    })
    return coal_mine_names.slice(0, 5);
  }

  const getHealthcareProviderNames = (close_healthcare_providers: number[]) => {
    if(close_healthcare_providers == undefined){
      return [];
    }
    let healthcare_provider_names: string[] = [];
    close_healthcare_providers.forEach((healthcare_provider_idx) => {
      let healthcare_provider : any = healthcareProviders.find((healthcare_provider : any) => healthcare_provider.idx === healthcare_provider_idx)
      if (healthcare_provider !== undefined) {
        healthcare_provider_names.push(healthcare_provider.name);
      }
    })
    return healthcare_provider_names.slice(0, 5);
  }

  function filterApi() {
    setCurrentPage(1)
    // url add ons for all the filters/sorts
    const stateURL = stateFilter === "" ? "" : "&state=" + stateFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by=" + sort_by_map[sortByProperty];
    const hcareMatch : any =  hcareFilter === "" ? "" : healthcareProviders.find((hcare : any) => {
      return hcare.name === hcareFilter;
    });

    let hcareURL = ""
    if (hcareMatch != undefined && hcareMatch != ""){
      hcareURL = "&close_healthcare_providers=" + [hcareMatch.idx];
    }
    // get new coal mines based on filter
    client.get("counties/" + currentPage.toString() + "?" + stateURL + sortByURL + hcareURL).then((response) => {
      setCounties(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
      // console.log(response.data.slice(1, 11));
    })
  
  }

  // if filter resulted in no results, set healthcareProviders to empty array
  if (counties.length === 1 && counties[0].length === 0) {
    setCounties([])
    setCurrentPage(0)
  }
  return (
    <Container>
      <Container className="container text-center mt-5 mb-4">
        <h1>Counties</h1>
        <p>{numCounties} Counties</p>
      </Container>

      <Container className="container text-center mt-5 mb-4">
        <Row>
        <SearchBar model="county" handleSearch={handleSearch}/>
          <Col>
            <FilterDropdown 
            name="Filter by State" 
            id="state-filter" 
            options={possible_states} 
            setFilter={setStateFilter}></FilterDropdown>
          </Col>
          <Col>
            <FilterDropdown
              name='Filter by Healthcare Provider'
              id='coal-rank-filter'
              options={possible_hcare}
              setFilter={setHcareFilter}></FilterDropdown>
          </Col>
          <Col> 
            <FilterDropdown
            name='Sort by ...'
            id='sort-by-name'
            options={['County Name', 'Population', 'Median Income']}
            setFilter={setSortByProperty}></FilterDropdown>
          </Col>
        </Row>
        <Row className='mt-4 '>
          <Col></Col>
          <Col><button className="btn btn-primary size-small"onClick={filterApi}>Apply</button></Col>
          <Col></Col>
        </Row>
      </Container>

      <Container> 
      <PaginationBar currentPage={currentPage} totalNumPages={totalNumPages} prevPage={prevPage} nextPage={nextPage} lastPage={() => setCurrentPage(totalNumPages)} firstPage={() => setCurrentPage(1)} setPage={setCurrentPage}></PaginationBar> 
          { /* generate card for each county in counties */}
          <Row
            xl={3}
            lg={3}
            md={3}
            sm={2}
            xs={1}
            className="d-flex g-1 p-1"
          >
            {counties.map((county : any) => {
              return (
                  <Col key={county.idx} className="d-flex align-items-stretch">
                    <CountiesCard
                      title={county.county} 
                      population={county.population}
                      state={county.state}
                      median_income={county.median_income}
                      coal_mines={getCoalMineNames(county.close_coal_mines).join(', ')}
                      healthcare_providers={getHealthcareProviderNames(county.close_healthcare_providers).join(', ')}
                      imgSrc={findUrlByName(county.county + " county, " + county.state)}
                      counties_link={'/counties/'+county.idx} 
                      highlight_term={county.hasOwnProperty('match_str') ? county.match_str[0] : ""}></CountiesCard>
                  </Col>
              )
            })}
          </Row>
      </Container>  
    </Container>
  )
}

export default Counties