import 'bootstrap/dist/css/bootstrap.min.css';  
import React, { useState, useEffect } from 'react';
import "./page.css";
import Link from 'next/link';

export default function Home() {
  return (
    <div>
      <div className="bg-image">
          <h1 className="bg-text">This is the coal reality.</h1>
      </div>
      <br></br>

      <div className="button-container">
        <Link href="/coal-mines" className="instance-button">
              <div className="instance-title">Coal Mines</div>
              <div className="instance-attributes">County, Status, Mining Method, etc.</div>
        </Link>

        <Link href="/counties" className="instance-button">
            <div className="instance-title">Counties</div>
            <div className="instance-attributes">Population, State, Median Income, etc.</div>
        </Link>

        <Link href="/healthcare-providers" className="instance-button">
            <div className="instance-title">Healthcare Providers</div>
            <div className="instance-attributes">Name, County, Physical Address, etc.</div>
        </Link>
      </div>

      <br></br>

      <div>
        <h1 className='overview-title-text'>What is the Coal Reality?</h1>
        <p className='overview-content-text'>
          The website will answer important questions related to coal mining and its impact, such as identifying coal mines in the Appalachian region, locating nearby healthcare providers for those affected by mining-related health issues, identifying affected counties in the region, and discovering which diseases are supported by specific healthcare organizations in Appalachia. This project will provide a valuable resource for residents and stakeholders seeking information on coal mining&apos;s effects on health and the environment, while also aiding individuals in finding appropriate healthcare support in affected areas.
        </p>
        <p className='overview-content-text'>
          Coal mining has many environmental impacts, including water and air pollution. The mining process can lead to acid mine drainage along with releasing methane and coal dust. Surface mining also removes large amounts of topsoil with can lead to land degradation. This can lead to negative heath impacts for miners and those living near mines. Some of the health impacts include respitory disease and exposure to carcinoginic compounds. While these impacts are severe, efforts are being made globally to counter these effects. This includes implementation of cleaner coal technologies and community engagement.
        </p>
      </div>
    </div>
    
  )
}