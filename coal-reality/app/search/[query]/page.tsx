'use client'

import 'bootstrap/dist/css/bootstrap.min.css';  
import { useParams } from 'next/navigation';
import React, { useState, useEffect } from 'react';
import { searchWebsite } from '../../utils/api';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { Container, Row, Col } from 'react-bootstrap';
import CoalMineCardSearch from '../CoalMineCardSearch';
import CountiesCardSearch from '../CountiesCardSearch';
import HealthCareCardSearch from '../HealthCareCardSearch';

const SearchPage = () => {
    const [activeTab, setActiveTab] = useState("coal_mine");
    const [coalMines, setCoalMines] = useState([]);
    const [counties, setCounties] = useState([]);
    const [healthcareProviders, setHealthcareProviders] = useState([]);
    const [loading, setLoading] = useState(true);
    const { query } = useParams();

    useEffect(() => {
        const searchResults = async () => {
            const data = await searchWebsite(query);
            console.log(data);
            // no data 
            if(data.length === 0 && data[0].model.length === 0) {
                setCoalMines([]);
                setCounties([]);
                setHealthcareProviders([]);
            } else {
                setCoalMines(data[0].models.filter((model : any) => model.hasOwnProperty('mineName')))
                setCounties(data[0].models.filter((model : any) => model.hasOwnProperty('county')))
                setHealthcareProviders(data[0].models.filter((model : any) => model.hasOwnProperty('name')))
            }
            // setCoalMines(data["coal_mine"]);
            // setCounties(data["counties"]);
            // setHealthcareProviders(data["healthcare_providers"]);
            // setLoading(false);
        };
        searchResults();
    }, [query]);
    
    const handleTabClick = (tabName: any) => {
        setActiveTab(tabName);
    };
    return (
        <Container>
            <Container className='text-center mt-4'>
                <h1>Search Results</h1>
            </Container>
            <Container className="mb-3 mt-5 p-0 w-100">
                <Tabs
                defaultActiveKey="coal-mines"
                className="mb-3 mt-5"
                fill
                >
                    <Tab eventKey="coal-mines" title="Coal Mine Results">
                        <Container> 
                            <Container className='text-center'>
                                <p>{coalMines.length} Coal Mine Results</p>
                            </Container>
                            <Row
                            xl={3}
                            lg={3}
                            md={3}
                            sm={2}
                            xs={1}
                            className="d-flex g-1 p-1"
                            >
                            {coalMines.map((coal_mine : any) => {
                                return (
                                    <Col key={coal_mine.idx} className="d-flex align-items-stretch">
                                    <CoalMineCardSearch
                                        title={coal_mine.mineName}
                                        prop1={""}
                                        prop2={coal_mine.coalRankDescription}
                                        prop3={coal_mine.average_employees}
                                        prop4={coal_mine.mineStatusDescription}
                                        prop5={coal_mine.production}
                                        prop6={""}
                                        imgSrc={""}
                                        coal_mine_link={`/coal-mines/${coal_mine.idx}`}
                                        highlight_term={coal_mine.hasOwnProperty('match_str') ? coal_mine.match_str[0] : ""}
                                        ></CoalMineCardSearch>
                                    </Col>
                                )
                            })}    
                            </Row>  
                        </Container> 
                    </Tab>
                    <Tab eventKey="counties" title="County Results">
                        <Container className='text-center'>
                            <p>{counties.length} Counties Results</p>
                        </Container>
                        <Container> 
                            <Row
                                xl={3}
                                lg={3}
                                md={3}
                                sm={2}
                                xs={1}
                                className="d-flex g-1 p-1"
                            >
                                {counties.map((county : any) => {
                                return (
                                    <Col key={county.idx} className="d-flex align-items-stretch">
                                        <CountiesCardSearch
                                        title={county.county} 
                                        population={county.population}
                                        state={county.state}
                                        median_income={county.median_income}
                                        coal_mines={""}
                                        healthcare_providers={""}
                                        imgSrc={""}
                                        counties_link={'/counties/'+county.idx} 
                                        highlight_term={county.hasOwnProperty('match_str') ? county.match_str[0] : ""}></CountiesCardSearch>
                                    </Col>
                                )
                                })}
                            </Row>
                        </Container> 
                    </Tab>
                    <Tab eventKey="healthcare-providers" title="Healthcare Provider Results">
                        <Container className='text-center'>
                            <p>{healthcareProviders.length} Healthcare Provider Results</p>
                        </Container>
                        <Container>  
                            <Row
                                xl={3}
                                lg={3}
                                md={3}
                                sm={2}
                                xs={1}
                                className="d-flex g-1 p-1"
                                >  
                                {healthcareProviders.map((healthcare_provider : any) => {
                                    return (
                                        <Col key={healthcare_provider.idx} className="d-flex align-items-stretch">
                                        <HealthCareCardSearch
                                            title={healthcare_provider.name}
                                            county={""}
                                            address={healthcare_provider.street_address}
                                            bed_count={healthcare_provider.hospital_bed_count}
                                            chrch_affl_f={healthcare_provider.chrch_affl_f}
                                            medicare_provider_number={healthcare_provider.medicare_provider_number}
                                            coal_mines={""}
                                            imgSrc={""}
                                            healthcare_provider_link={"./healthcare-providers/" + healthcare_provider.idx}
                                            highlight_term={healthcare_provider.hasOwnProperty('match_str') ? healthcare_provider.match_str[0] : ""}></HealthCareCardSearch>
                                        </Col>
                                    )
                                })}
                            </Row>  
                        </Container>
                    </Tab>
                </Tabs>
            </Container>
        </Container>
    )
}

export default SearchPage;