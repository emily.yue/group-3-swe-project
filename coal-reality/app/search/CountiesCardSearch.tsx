'use client'
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./CountiesCardSearch.css"
import { Button } from 'react-bootstrap'
import { highlightText } from '../utils/highlight';


const CountiesCardSearch = ({title, 
                      population, 
                      state, 
                      median_income, 
                      coal_mines, 
                      healthcare_providers, 
                      imgSrc,
                      counties_link,
                      highlight_term} : {
                      title: string; 
                      population: string;
                      state: string;
                      median_income: string;
                      coal_mines: string;
                      healthcare_providers: string; 
                      imgSrc: string;
                      counties_link: string;
                      highlight_term: string}) => {

    return (
        <Card className="ms-2 me-2 mb-4 mt-4 flex-fill" data-testid="county-card">
            {/* <Card.Img variant='top' src={imgSrc} alt="Card image cap" /> */}
            <Card.Body>
                <Card.Title>{highlightText(title, highlight_term)}</Card.Title>
                <Card.Text>
                    <b>Population:</b> {highlightText(population, highlight_term)}
                    <br/>
                    <b>State:</b> {highlightText(state, highlight_term)}
                    <br/>
                    <b>Median Income:</b> {highlightText(median_income, highlight_term)}
                    <br/>
                    {/* <b>Coal Mines:</b> {highlightText(coal_mines, highlight_term)}
                    <br/> */}
                    {/* <b>Healthcare Providers:</b> {highlightText(healthcare_providers, highlight_term)}
                    <br/> */}
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={counties_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default CountiesCardSearch