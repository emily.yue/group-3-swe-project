'use client'
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./HealthCareCardSearch.css"
import { Button } from 'react-bootstrap'
import { highlightText } from '../utils/highlight';


const HealthCareCardSearch = ({title, 
                      county,
                      address, 
                      bed_count, 
                      chrch_affl_f, 
                      medicare_provider_number,
                      coal_mines, 
                      imgSrc,
                      healthcare_provider_link,
                      highlight_term} : {
                      title: string; 
                      county: string; address: string; bed_count: string; chrch_affl_f: string; medicare_provider_number: string; coal_mines: string;
                      imgSrc: string;
                      healthcare_provider_link: string;
                      highlight_term: string}) => {

    return (
        <Card className="ms-2 me-2 mb-4 mt-4 flex-fill" data-testid="healthcare-provider-card">
            {/* <Card.Img variant='top' src={imgSrc} alt="Card image cap" /> */}
            <Card.Body>
                <Card.Title>{highlightText(title, highlight_term)}</Card.Title>
                <Card.Text>
                    {/* <b>County:</b> {highlightText(county, highlight_term)}
                    <br/> */}
                    <b>Physical Address:</b> {highlightText(address, highlight_term)}
                    <br/>
                    <b>Hospital Bed Count:</b> {highlightText(bed_count, highlight_term)}
                    <br/>
                    <b>Church Affliation Yes or No?:</b> {highlightText(chrch_affl_f, highlight_term)}
                    <br/>
                    <b>Medicare Provider Number:</b> {highlightText(medicare_provider_number, highlight_term)}
                    <br/>
                    {/* <b>Coal Mines:</b> {highlightText(coal_mines, highlight_term)}
                    <br/> */}
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={healthcare_provider_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default HealthCareCardSearch