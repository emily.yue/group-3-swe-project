'use client'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import HealthCareCard from './HealthCareCard'
import PaginationBar from '../components/paginationbar';
import FilterDropdown from '../components/filterdropdown';
import SearchBar from '../components/search-bar';
import { search_model } from '../utils/api';

// change later
import healthcare_providers_img from '../images/healthcare_providers_img.json';

function findUrlByName(name: string): string {
  const entry = healthcare_providers_img.find(item => item.name === name);
  if (entry == null) {
      return ""
  }
  return entry.url;
}

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  // baseURL:  "http://127.0.0.1:5000/api/"
  baseURL: "https://api.coal-reality.me/api"
});

const cards_per_page = 10;

// filtering options
const sort_by_map: Record<string, string> = {
  'Healthcare Provider Name': 'name',
  'Hospital Bed Count': 'hospital_bed_count',
  'Street Address' : 'street_address'
}
const possible_counties = ["Leslie", "Harlan", "Armstrong", "Nicholas", "Wetzel", "Daviess", "Stark", "Greene", "Claiborne", "Belmont", "Jefferson", "Floyd", "Logan", "Pike", "Washington", "Clearfield", "Mercer", "Livingston", "Barbour", "Lee", "Walker", "Muhlenberg", "Kanawha", "Marion", "Cambria", "Marshall", "Anderson", "Boone", "Grant", "Buchanan", "Fulton", "Choctaw", "Mineral", "Randolph", "Tazewell", "Bedford", "Monongalia", "Luzerne", "Elk", "Dauphin", "Wyoming", "Athens", "Ohio", "Russell", "Somerset", "Laurel", "Lincoln", "Wayne", "Lycoming", "Wise", "Butler", "Taylor", "Union", "Carbon", "Webster", "Perry", "Bibb", "Boyd", "Bell", "Indiana", "Clarion", "Allegheny", "Harrison", "Knox", "Johnson", "Northumberland", "Letcher", "Coshocton", "Morgan", "Jackson", "Columbia", "Lawrence", "Raleigh", "Dickenson", "Clay", "Allegany", "Campbell", "Lackawanna", "Montgomery", "Upshur", "Hopkins", "Berks", "Tuscarawas", "Shelby", "Fayette", "Cullman", "Schuylkill", "Westmoreland", "Venango"]

const HealthcareProviders = () => {
  const[healthcareProviders, setHealthcareProviders] = useState<any[]>([]); // change later to [] after api is set up
  const[coalMines, setCoalMines] = useState([]); // change later to [] after api is set up
  const[counties, setCounties] = useState([]); // change later to [] after api is set up
  const[numHealthcareProviders, setNumHealthcareProviders] = useState(healthcareProviders.length);
  const[currentPage, setCurrentPage] = useState(1);
  const[totalNumPages, setTotalNumPages] = useState(1);

  // filtering states
  const[churchAffFilter, setChurchAffFilter] = useState("");
  const[sortByProperty, setSortByProperty] = useState("");
  const[countyFilter, setCountyFilter] = useState("");

  const handleSearch = async (search_term : any) => {
    const search_data = await search_model(search_term, "healthcare_providers");
    // this is data retrieved
    setHealthcareProviders(search_data);
    setTotalNumPages(1);
  }

  useEffect(() => {
    client.get("coal_mines").then((response) => {
      setCoalMines(response.data);
    })
    client.get("counties").then((response) => {
      setCounties(response.data);
    })
  }, [])
  
  useEffect(() => {
    // client.get("healthcare_providers/"+currentPage.toString()).then((response) => {
    //   setHealthcareProviders(response.data.slice(1, 11));
    //   setTotalNumPages(response.data[0]); 
    // })
    const churchAffURL = churchAffFilter === "" ? "" : "&chrch_affl_f=" + churchAffFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by=" + sort_by_map[sortByProperty];
    const countyMatch : any =  countyFilter === "" ? "" : counties.find((county : any) => {
      return county.county === countyFilter;
    });
    let countyURL = ""
    if (countyMatch != undefined && countyMatch != ""){
      countyURL = "&close_county=" + countyMatch.idx;
    }
    client.get("healthcare_providers/"+currentPage.toString() + "?" + churchAffURL + sortByURL + countyURL).then((response) => {
      setHealthcareProviders(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
    })
  
  }, [currentPage])

  useEffect(() => {
    setNumHealthcareProviders(healthcareProviders.length);
  }, [healthcareProviders])

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }

  const nextPage = () => {  
    if (currentPage < totalNumPages) {
      setCurrentPage(currentPage + 1);
    }
  }

  const getCoalMineNames = (close_coal_mines: number[]) => {
    if(close_coal_mines == undefined){
      return [];
    }
    let coal_mine_names: string[] = [];
    close_coal_mines.forEach((coal_mine_idx) => {
      let coal_mine : any = coalMines.find((coal_mine : any) => coal_mine.idx === coal_mine_idx)
      if (coal_mine !== undefined) {
        coal_mine_names.push(coal_mine.mineName.toString());
      }
    })
    return coal_mine_names.slice(0, 5);
  }

  const getCountyName = (close_county: number) => {
    let county : any = counties.find((county : any) => county.idx === close_county)
    if(county !== undefined){
      return county.county;
    }
    return ""
  }

  function filterApi() {
    setCurrentPage(1)
    // url add ons for all the filters/sorts
    const churchAffURL = churchAffFilter === "" ? "" : "&chrch_affl_f=" + churchAffFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by=" + sort_by_map[sortByProperty];
    const countyMatch : any =  countyFilter === "" ? "" : counties.find((county : any) => {
      return county.county === countyFilter;
    });
    let countyURL = ""
    if (countyMatch != undefined && countyMatch != ""){
      countyURL = "&close_county=" + countyMatch.idx;
    }
    // get new coal mines based on filter
    console.log("healthcare_providers/"+currentPage.toString() + "?" + churchAffURL + sortByURL + countyURL)
    client.get("healthcare_providers/"+currentPage.toString() + "?" + churchAffURL + sortByURL + countyURL).then((response) => {
      setHealthcareProviders(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
    })
  }
 
  // if filter resulted in no results, set healthcareProviders to empty array
  if (healthcareProviders.length === 1 && healthcareProviders[0].length === 0) {
    setHealthcareProviders([])
    setCurrentPage(0)
  }
  return (
    <Container>
    <Container className="container text-center mt-5 mb-4">
      <h1>Health Care Providers</h1>
      <p>{numHealthcareProviders} Healthcare Providers</p>
    </Container>

    <Container className="container text-center mt-5 mb-4">
        <Row>
        <SearchBar model="healthcare_providers" handleSearch={handleSearch}/>
          <Col>
            <FilterDropdown 
            name="Filter by Church Affiliation"
            id="chrch_affl-filter" 
            options={['Y', 'N']} 
            setFilter={setChurchAffFilter}></FilterDropdown>
          </Col>
          <Col>
            <FilterDropdown
              name='Filter by County'
              id='county-filter'
              options={possible_counties.sort()}
              setFilter={setCountyFilter}></FilterDropdown>
          </Col>
          <Col> 
            <FilterDropdown
            name='Sort by ...'
            id='sort-by-name'
            options={['Healthcare Provider Name', 'Hospital Bed Count', 'Street Address']}
            setFilter={setSortByProperty}></FilterDropdown>
          </Col>
        </Row>
        <Row className='mt-4 '>
          <Col></Col>
          <Col><button className="btn btn-primary size-small"onClick={filterApi}>Apply</button></Col>
          <Col></Col>
        </Row>
    </Container>

    <Container>  
    <PaginationBar currentPage={currentPage} totalNumPages={totalNumPages} prevPage={prevPage} nextPage={nextPage} lastPage={() => setCurrentPage(totalNumPages)} firstPage={() => setCurrentPage(1)} setPage={setCurrentPage}></PaginationBar> 
      <Row
        xl={3}
        lg={3}
        md={3}
        sm={2}
        xs={1}
        className="d-flex g-1 p-1"
        >  
        {healthcareProviders.map((healthcare_provider : any) => {
            return (
                <Col key={healthcare_provider.idx} className="d-flex align-items-stretch">
                  <HealthCareCard
                    title={healthcare_provider.name}
                    county={getCountyName(healthcare_provider.close_county)}
                    address={healthcare_provider.street_address}
                    bed_count={healthcare_provider.hospital_bed_count}
                    chrch_affl_f={healthcare_provider.chrch_affl_f}
                    medicare_provider_number={healthcare_provider.medicare_provider_number}
                    coal_mines={getCoalMineNames(healthcare_provider.close_coal_mines).join(", ")}
                    imgSrc={findUrlByName(healthcare_provider.name)}
                    healthcare_provider_link={"./healthcare-providers/" + healthcare_provider.idx}
                    highlight_term={healthcare_provider.hasOwnProperty('match_str') ? healthcare_provider.match_str[0] : ""}></HealthCareCard>
                </Col>
            )
          })}
      </Row>  
    </Container>  
  </Container>
  )
}

export default HealthcareProviders