'use client'
import 'bootstrap/dist/css/bootstrap.min.css';  
import { Container } from "@chakra-ui/react";
import HealthcareProvidersInstance from '../HealthcareProvidersInstance'
import React, {useEffect, useState } from 'react'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'
import axios from 'axios'

// change to real data
import healthcare_providers_img from '../../images/healthcare_providers_img.json';

function findUrlByName(name: string): string {
  const entry = healthcare_providers_img.find(item => item.name === name);
  if (entry == null) {
      return ""
  }
  return entry.url;
}

const getCoalMines = (close_cloal_mines: number[], all_coal_mines : any[]) => {
    let coal_mines : any[] = []
    close_cloal_mines.forEach((coal_mine_idx) => {
      let coal_mine = all_coal_mines.find((coal_mine) => coal_mine.idx === coal_mine_idx)
      if (coal_mine !== undefined) {
        coal_mines.push(coal_mine);
      }
    })
    return coal_mines;
}


const getCounty = (close_county: number, all_counties : any[]) => {
    let county : any = all_counties.find((county : any) => county.idx === close_county)
    if(county !== undefined){
      return county;
    }
    return ""
}

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  baseURL: "https://api.coal-reality.me/api" // "http://127.0.0.1:5000/api/"
});



function HealthcareProviderPage({params}: {params: { id: string }}) {
    // const healthcare_provider = HealthcareProvidersInfo.find((healthcare_provider) => healthcare_provider.idx === parseInt(params.id))
    const[healthcare_provider, setHealthcareProvider] = useState<any>(null);
    const[coalMines, setCoalMines] = useState([]); // change later to [] after api is set up
    const[counties, setCounties] = useState([]); // change later to [] after api is set up 
    const[related_county, setRelatedCounty] = useState<any>(null);
    const[related_coal_mines, setRelatedCoalMines] = useState<any>([]);
    
    useEffect(() => {
        client.get("healthcare_provider/"+params.id.toString()).then((response) => {
            setHealthcareProvider(response.data);
        })
        client.get("coal_mines").then((response) => {
            setCoalMines(response.data);
        })
        client.get("counties").then((response) => {
            setCounties(response.data);
        })

    }, [])

    useEffect(() => {
        if(healthcare_provider !== undefined && healthcare_provider !== null) {
            setRelatedCoalMines(getCoalMines(healthcare_provider.close_coal_mines, coalMines));
            setRelatedCounty(getCounty(healthcare_provider.close_county, counties));
        }
    }, [healthcare_provider, coalMines, counties])

    if (healthcare_provider === null || related_county === null || coalMines.length == 0 || counties.length == 0) {
        return (
            // <Container>
            //     <h1>Healthcare Provider Not Found</h1>
            // </Container>
            <div></div>
        )
    }

    return (
        <Container>
            <HealthcareProvidersInstance 
            title={healthcare_provider.name}
            prop1={related_county.county}
            prop2={healthcare_provider.street_address}
            prop3={healthcare_provider.hospital_bed_count.toString()}
            prop4={healthcare_provider.chrch_affl_f}
            prop5={healthcare_provider.medicare_provider_number.toString()}
            prop6={related_coal_mines.map(function(related_coal_mine : any) {return related_coal_mine.mineName.toString();}).join(', ')}
            imgSrc={findUrlByName(healthcare_provider.name)}
            mapSrc={'https://www.google.com/maps/embed/v1/place?key=AIzaSyDSFN1kt4StJE_0zKTdX8uC9-T8489W-2w&q=' + healthcare_provider.street_address}></HealthcareProvidersInstance>

            <Row>
                <Col className='ms-4'>
                    <Card className="ms-auto me-auto mb-4 mt-4">
                            <Card.Header>Coal Mines:</Card.Header>
                            <Card.Body>
                                {related_coal_mines.map((coal_mine : any) => {
                                    return (
                                        <Link key={coal_mine.idx} href={'../coal-mines/' + coal_mine.idx.toString()}>
                                            <Button className='w-100 mb-4'>
                                                {coal_mine.mineName}
                                            </Button>
                                        </Link> 
                                    )
                                })}
                            </Card.Body>
                    </Card>
                </Col>
                <Col className="me-4">
                    <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>Counties:</Card.Header>
                        <Card.Body>
                            <Link key={related_county.idx} href={'../counties/' + related_county.idx.toString()}>
                                <Button className='w-100 mb-4'>
                                    {related_county.county}
                                </Button>
                            </Link> 
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default HealthcareProviderPage;