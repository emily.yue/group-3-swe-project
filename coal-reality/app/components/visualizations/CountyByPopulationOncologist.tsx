import React, {useState, useEffect } from 'react';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Label } from 'recharts';

interface OncologistCountyMap {
    [key: string]: number;
}

interface PopulationCountyMap {
    [key: string]: number;
}

interface Oncologist {
    address: string;
    city: string;
    clinic_name: string;
    county: string;
    gender: string;
    name: string;
    npi: string;
    phone_number: string;
    specialization: string;
    zip_code: string;
}

interface Census {
    city: string;
    county: string;
    geo_id: string;
    name: string;
    npi: string;
    population: string;
    trial_id: string;
    zip_code: string;
}

interface GraphData {
    county: string;
    oncologist: number;
    population: number;
}

const CountyByPopulationOncologist = () => {

    const [data, setData] = useState<GraphData[]>([]);

    useEffect(() => {
        const fetchData = async () => {
          const oncUrl = "https://backend.cancer-care.me/oncologist_all";
    
          const oncResp = await fetch(oncUrl)
          const oncologistData: Oncologist[] = await oncResp.json()
    
          const oncologistCountyMap: OncologistCountyMap = {};
    
          oncologistData.forEach((item: Oncologist) => {
            let { county } = item;
            county = county.toLowerCase();
            if (!oncologistCountyMap[county]) {
                oncologistCountyMap[county] = 0;
            }
            oncologistCountyMap[county] += 1;
            console.log("oncologist: " + county);
          });

          const popUrl = "https://backend.cancer-care.me/census_tract_all";
    
          const popResp = await fetch(popUrl)
          const popData: Census[] = await popResp.json()
    
          const popCountyMap: PopulationCountyMap = {};
    
          popData.forEach((item: Census) => {
            //console.log(item);
            const { population, county } = item;
            if(!popCountyMap[county]) {
                popCountyMap[county] = 0;
            }
            popCountyMap[county] += Number(population);
          });

          const mergedData = Object.keys(popCountyMap).map(county => {
            console.log("county: " + county);
            return { 
              county, 
              population: (popCountyMap[county])/10000, 
              oncologists: oncologistCountyMap[county] // Assuming some counties might not have oncologist data
            };
          });

          const scatterData = mergedData.map(({ county, population, oncologists }) => ({
            county: county,
            oncologist: oncologists,
            population: population
          }));
    
          setData(scatterData);
        };
    
        fetchData();
      }, []);
      
      return (
        <div>
          <h3 style={{color: 'black'}} className="p-5 text-center">Population vs. Oncologists Per County</h3>
          <ResponsiveContainer width={1000} height={400}>
            <ScatterChart
              margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
            >
              <CartesianGrid />
                <YAxis type="number" dataKey="population" name="Population">
                    <Label value="Population (in thousands)" offset={20} angle={-90} position='insideBottomLeft' />
                </YAxis>
                <XAxis type="number" dataKey="oncologist" name="Oncologists">
                    <Label value="Number of Oncologists" offset={-15} position='insideBottomLeft' />
                </XAxis>
                
                <Tooltip/>
              <Scatter name="Counties" data={data} fill='blue' />
            </ScatterChart>
          </ResponsiveContainer>
        </div>
      );
    };

export default CountyByPopulationOncologist;