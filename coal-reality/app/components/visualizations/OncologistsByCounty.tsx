import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { PieChart, Pie, Tooltip, ResponsiveContainer } from "recharts";

interface CountyMap {
  [key: string]: number;
}

interface Oncologist {
  address: string;
  city: string;
  clinic_name: string;
  county: string;
  gender: string;
  name: string;
  npi: string;
  phone_number: string;
  specialization: string;
  zip_code: string;
}

interface ChartData {
  name: string;
  count: number;
}

const OncologistsByCounty = () => {
  const [data, setData] = useState<ChartData[]>([]);


  useEffect(() => {
    const fetchData = async () => {
      const url = "https://backend.cancer-care.me/oncologist_all";

      const response = await fetch(url)
      const oncologistData: Oncologist[] = await response.json()

      const countyMap: CountyMap = {};

      oncologistData.forEach((item: Oncologist) => {
        const { county } = item;
        if (!countyMap[county]) {
          countyMap[county] = 0;
        }
        countyMap[county] += 1;
      });

      const chartData = Object.keys(countyMap).map((key) => ({
        name: key,
        count: countyMap[key]
      }));

      setData(chartData);
    };

    fetchData();
  }, []);
  
  return (
    <Container fluid="md">
      <Row style={{ width: "100%", height: 600 }}>
        <h3 style={{color: 'black'}} className="p-5 text-center">Number of Oncologists by County</h3>
        <Col>
          <ResponsiveContainer width="100%" height="100%">
            <PieChart width={500} height={500}>
              <Pie
                dataKey="count"
                isAnimationActive={false}
                data={data}
                cx="50%"
                cy="50%"
                outerRadius={200}
                fill="#8884d8"
                label
              />
              <Tooltip />
            </PieChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </Container>
  );


};

export default OncologistsByCounty;
