import React, { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import { 
    BarChart, 
    Bar, 
    XAxis, 
    YAxis, 
    CartesianGrid,
    Cell,
    Tooltip } from 'recharts';

interface TrialCount {
    [key: string]: number;
}

interface ClinicalTrials {
    address: string;
    city: string;
    contact_email: string;
    contact_phone: string;
    county: string;
    geo_id: string; 
    npi: string;
    purpose: string; 
    recruiting_status: string;
    title: string;
    trial_id: string;
}

interface ChartData {
    name: string;
    count: number;
}

const ClinicalTrialsStatus = () => {

    const [data, setData] = useState<ChartData[]>([]);

    useEffect(() => {
        const fetchData = async() => {
            const url = 'https://backend.cancer-care.me/trial_all'
            const response = await fetch(url);
            const responseData: ClinicalTrials[] = await response.json();

            const trialCount: TrialCount = {};
            responseData.forEach((item: ClinicalTrials) => {
                const { recruiting_status }  = item;
                if (!trialCount[recruiting_status]) {
                    trialCount[recruiting_status] = 0;
                }
                trialCount[recruiting_status] += 1;
            });

            const chartData = Object.keys(trialCount).map((key) => ({
                name: key,
                count: trialCount[key]
            }));

            setData(chartData);
        };

        fetchData();
    }, []);

    const getShadeOfRed = (index : any) => {
        const redValue = 255;
        const greenBlueValue = 100 + (index * 30) % 155; // Adjust this formula as needed
        return `rgb(${redValue}, ${greenBlueValue}, ${greenBlueValue})`;
      };
    
      return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Row>
            <h3 style={{color: 'black'}} className="p-5 text-center">Clinical Trials Status<br></br></h3>
            <Col>
              <BarChart
                width={1000}
                height={300}
                data={data}
                margin={{
                  top: 20, right: 20, left: 20, bottom:20,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Bar dataKey="count">
                  {
                    data.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={getShadeOfRed(index)} />
                    ))
                  }
                </Bar>
              </BarChart>
            </Col>
          </Row>
        </div>
      );
    };

export default ClinicalTrialsStatus;