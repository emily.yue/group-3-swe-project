'use client'

import 'bootstrap/dist/css/bootstrap.min.css';  
import React from 'react';
import Link from 'next/link';
// import { useNavigate } from 'react-router-dom';
import { useRouter } from 'next/navigation';
import styles from './Navbar.module.css';
import { Form } from 'react-bootstrap';

const Navbar = () => {

  // const navigate = useNavigate();
  const router = useRouter();

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const input = event.target.querySelector('input');
    // navigate(`/search/${input.value}`);
    router.push(`/search/${input.value}`);
  };

  return (
    <nav className={styles.navbar}>
      <ul className={styles.navList}>
      <li className={styles.navItem}>
          <Link href="/">Home</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/about">About</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/coal-mines">Coal Mines</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/counties">Counties</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/healthcare-providers">Healthcare Providers</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/visualizations">Visualizations</Link>
        </li>
        <li className={styles.navItem}>
          <Link href="/provider-visualizations">Provider Visualizations</Link>
        </li>
        <li className=''>
          <Form onSubmit={handleSubmit} className="d-flex">
              <Form.Control className="custom" type="search" placeholder="Search" />
          </Form>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
