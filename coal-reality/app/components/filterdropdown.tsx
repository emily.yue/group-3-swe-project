import 'bootstrap/dist/css/bootstrap.min.css';  
import Form from 'react-bootstrap/Form';

const FilterDropdown = ({name, id, options, setFilter} : {name : string; id : string; options: any[]; setFilter : (text : string) => void} ) => {
    const handleChange = () => {
        var e : any = document.getElementById(id);
        if (e == null){
            return;
        }
        if (e.value === "None") {
          setFilter("");
        } else {
            console.log(e.options[e.selectedIndex].text);
            setFilter(e.options[e.selectedIndex].text);
        }
    }
    return (
        <>
            <Form.Select 
             aria-label="Filter"
             onChange={handleChange}
             id={id}
             >
                <option value="None" key={name}>{name}</option>
                {options.map((option, index) => {
                    return(<option key={index} value={option}>{option}</option>)
                })}
            </Form.Select>
        </>
    )
}

export default FilterDropdown;