'use client'

import 'bootstrap/dist/css/bootstrap.min.css';  
import React, { useState } from 'react';

const SearchBar = ({ model, handleSearch } : {model : any, handleSearch: any}) => {
        const handleChange = (event : any) => {
            setMessage(event.target.value);
        }
        const [message, setMessage] = useState('');
    
        return (
            <div>
                <input
                    placeholder= {'Search ' + model}
                    aria-label={'Search ' + model}
                    onChange={handleChange}
                    value={message}
                    style={{width: '40%', height: '40px', borderRadius: '5px', marginBottom: '10px', border: '1px solid #ced4da', padding: '10px'}}
                />
                <button
                    className="btn btn-primary size-small ms-2"
                    style={{height: '40px', marginBottom: '2px'}}
                    onClick={() => {
                        if (handleSearch) {
                            handleSearch(message);
                        }
                        // setMessage("");
                    }}>
                    Search
                </button>
            </div>
        );
    };
    
    export default SearchBar;