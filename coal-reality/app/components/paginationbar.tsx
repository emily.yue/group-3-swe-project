import 'bootstrap/dist/css/bootstrap.min.css';  
import { Pagination } from "react-bootstrap"

const PaginationBar = ({currentPage, totalNumPages, prevPage, nextPage, lastPage, firstPage, setPage} : {currentPage : number; totalNumPages : number; prevPage : () => void; nextPage : () => void; lastPage : () => void; firstPage : () => void; setPage : (num : number) => void}) => {
    const calculatePageNumbers = () => {
        const siblingCount = 3;
        const totalPageItems = siblingCount + 5;

        if(totalPageItems >= totalNumPages) {
            const arr = [];
            for(let i = 1; i < totalNumPages + 1; i++) {
                arr.push(i);
            }
            return arr;
        }

        const leftSiblingIndex = Math.max(currentPage - siblingCount, 1);
        const rightSiblingIndex = Math.min(currentPage + siblingCount, totalNumPages);
        const shouldShowLeftDots = leftSiblingIndex > 2;
        const shouldShowRightDots = rightSiblingIndex < totalNumPages - 2;
        const firstPageIndex = 1;
        const lastPageIndex = totalNumPages;

        if(!shouldShowLeftDots && shouldShowRightDots) {
            let leftItemCount = 3 + 2 * siblingCount;
            const leftRange = [];
            for(let i = 1; i < leftItemCount + 1; i++) {
                leftRange.push(i);
            }
            return [...leftRange, "...", totalNumPages];
        }

        if(shouldShowLeftDots && !shouldShowRightDots) {
            let rightItemCount = 3 + 2 * siblingCount;
            const rightRange = [];
            for(let i = totalNumPages - rightItemCount + 1; i < totalNumPages + 1; i++) {
                rightRange.push(i);
            }
            return [firstPageIndex, "...", ...rightRange];
        }

        if(shouldShowLeftDots && shouldShowRightDots) {
            let middleRange = [];
            for(let i = leftSiblingIndex; i < rightSiblingIndex + 1; i++) {
                middleRange.push(i);
            }
            return [firstPageIndex, "...", ...middleRange, "...", lastPageIndex];
        }
    
    }

    const values = calculatePageNumbers();
    if(values === undefined) return (<></>);

    return (
        <Pagination className="justify-content-center">
            <Pagination.First onClick={firstPage}/>
            <Pagination.Prev onClick={prevPage} />
            {values.map((currPageVal) => {
                if(currPageVal === "..."){
                    return (
                        <Pagination.Ellipsis key={currPageVal}/>
                    )
                }
                if(currPageVal === currentPage) {
                    return (
                        <Pagination.Item key={currPageVal} onClick={() => setPage(Number(currPageVal))} active>{currPageVal}</Pagination.Item>
                    );
                }
                return (
                    <Pagination.Item key={currPageVal} onClick={() => setPage(Number(currPageVal))}>{currPageVal}</Pagination.Item>
                );
            })}
            {/* <Pagination.Item>{currentPage} out of {totalNumPages}</Pagination.Item> */}
            <Pagination.Next onClick={nextPage}/>
            <Pagination.Last onClick={lastPage}/>
        </Pagination>
    )
}

export default PaginationBar;