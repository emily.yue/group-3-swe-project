'use client'
import React, { useState } from 'react';
import './globals.css'
import { Inter } from 'next/font/google'
import NavBar from './components/navbar'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// import SearchPage from './search/page'

const inter = Inter({ subsets: ['latin'] })



export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
      <title>Appalachia Coal Reality</title>
        <link
          rel="icon"
          type="image/png"
          href="https://static.wikia.nocookie.net/minecraft/images/d/de/CoalNew.png"
        />
      </head>
      <body className={inter.className}>
        {/* <Router> */}
          <NavBar/>
          {/* <div>
            <Routes>
            <Route path="/search/:query" element={<SearchPage />} />
            </Routes>
          </div> */}
        {/* </Router> */}
        {children}
      </body>
    </html>
  )
}
