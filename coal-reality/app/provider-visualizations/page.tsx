'use client'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import OncologistsByCounty from "../components/visualizations/OncologistsByCounty"
import ClinicalTrialsStatus from '../components/visualizations/ClinicalTrialsStatus';
import CountyByPopulationOncologist from '../components/visualizations/CountyByPopulationOncologist';

function ProviderVisualizations() {

  return (
    <Container className='text-center'>
        <h1 className='mt-4'>Provider Visualizations</h1>
        <h4>Cancer Care</h4>
            <Container className="container text-center mt-3 mb-4">
              <OncologistsByCounty />
              <ClinicalTrialsStatus/>
              <CountyByPopulationOncologist />
            </Container>
    </Container>
  );
}

export default ProviderVisualizations