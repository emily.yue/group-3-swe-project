'use client'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { PieChart, Pie, BarChart, Cell,  Bar, LineChart, ScatterChart, Label, Scatter, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  baseURL: "https://api.coal-reality.me/api"  
  // baseURL: "http://127.0.0.1:5000/api"
});


function Visualizations() {
  const[dataVisual1, setDataVisual1] = useState<any[]>([]);
  const[dataVisual2, setDataVisual2] = useState<any[]>([]);
  const[dataVisual3, setDataVisual3] = useState<any[]>([]);


  useEffect(() => {
    createVisual1();
    createVisual2();
    createVisual3();
  }, [])

  // visual 1
  const createVisual1 = async () => {
    // get data
    let response = await client.get("coal_mines");
    setDataVisual1(response.data);
  }

  // visual 2
  const createVisual2 = async () => {
    // get data
    let response = await client.get("counties");
    // sort data
    // create dictionary
    let countState : any = {};
    response.data.forEach((item: any) => {
      if (!countState[item.state]) {
        countState[item.state] = [];
      }
      countState[item.state].push(item.median_income);
    })
    const countStateArray = Object.keys(countState).map((key) => ({
      name: key,
      count: countState[key].reduce((a: any, b: any) => a + b, 0) / countState[key].length
    }));
    setDataVisual2(countStateArray);
  }

  // visual 3
  const createVisual3 = async () => {
    // get data
    let response = await client.get("healthcare_providers");
    
    // sort data
    // create dictionary
    let countChurchAffliation : any = {};
    response.data.forEach((item: any) => {
      if (!countChurchAffliation[item.chrch_affl_f]) {
        countChurchAffliation[item.chrch_affl_f] = 0;
      }
      countChurchAffliation[item.chrch_affl_f] += 1;
    })
    const countChurchAffliationArray = Object.keys(countChurchAffliation).map((key) => ({
      name: key,
      count: countChurchAffliation[key]
    }));
    setDataVisual3(countChurchAffliationArray);

  }


  return (
    <div className='text-center'>
        <h1 className='mt-4 mb-5'>Visualizations</h1>
        <Container className="container text-center mt-3 mb-4">
          <div>
            <h3>Production vs Average Employees in Coal Mines</h3>
            <ResponsiveContainer width={1300} height={400}>  
              <ScatterChart
                margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
              >
                <CartesianGrid />
                <YAxis dataKey="production" type="number" name="Production">
                  {/* <Label value="Production" offset={0} angle={-90} position='insideBottomLeft' /> */}
                </YAxis>
                <XAxis dataKey="average_employees" type="number" name="Avg Employees">
                  {/* <Label value="Average Employees" offset={-5} position='insideBottomLeft' /> */}
                </XAxis>
                <Tooltip cursor={{ strokeDasharray: '2 2' }} />
                <Scatter name="Coal Mines" data={dataVisual1} fill="#8884d8" />
              </ScatterChart>
            </ResponsiveContainer>
          </div>

          <div>
            <h3>Average Median Income of County by State</h3>
            <BarChart
                width={1300}
                height={500}
                data={dataVisual2}
                margin={{
                  top: 20, right: 20, left: 20, bottom:20,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Bar dataKey="count">
                  {
                    dataVisual2.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={'red'} />
                    ))
                  }
                </Bar>
              </BarChart>
          </div>

          <Container fluid='md'>
            <Row style={{ width: "100%", height: 600 }}>
            <h3>Chruch Affiliation of Healthcare Providers</h3>
              <Col>
              <ResponsiveContainer width="100%" height="100%">
                <PieChart width={500} height={500}>
                  <Pie
                    dataKey="count"
                    isAnimationActive={false}
                    data={dataVisual3}
                    cx="50%"
                    cy="50%"
                    outerRadius={200}
                    fill="#8884d8"
                    label
                  />
                  <Tooltip />
                </PieChart>
              </ResponsiveContainer>
              </Col>
            </Row>
          </Container>
        </Container>
    </div>
  );
}

export default Visualizations