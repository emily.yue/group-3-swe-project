'use client'
import 'bootstrap/dist/css/bootstrap.min.css';  
import { Container } from "@chakra-ui/react";
import CoalMineInstance from '../CoalMineInstance'
import React, {useEffect, useState } from 'react'
import Card from 'react-bootstrap/Card' 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { Button } from 'react-bootstrap'
import axios from 'axios'

// change to real data
import  coal_mine_img from '../../images/coal_mine_img.json'

function findUrlByName(name: string): string {
    const entry = coal_mine_img.find(item => item.name === name);
    if (entry == null) {
        return ""
    }
    return entry.url;
}

const getCounty = (close_county: number, all_counties : any[]) => {
    let county : any = all_counties.find((county : any) => county.idx === close_county)
    if(county !== undefined){
      return county;
    }
    return "Hello"
}

const getHealthcareProviders = (close_healthcare_providers: number[], all_healthcare_providers : any[]) => {
    let healthcare_providers: any[] = [];
    close_healthcare_providers.forEach((healthcare_provider_idx) => {
        let healthcare_provider = all_healthcare_providers.find((healthcare_provider) => healthcare_provider.idx === healthcare_provider_idx)
        if (healthcare_provider !== undefined) {
            healthcare_providers.push(healthcare_provider);
        }
    })
    return healthcare_providers;
}

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  baseURL: "https://api.coal-reality.me/api"
});


function CoalMinePage({params}: {params: { id: string }}) {
    // const coal_mine = CoalMinesInfo.find((coal_mine) => coal_mine.idx === parseInt(params.id));    
    
    const[coal_mine, setCoalMine] = useState<any>(null);
    const[counties, setCounties] = useState([]); // change later to [] after api is set up 
    const[healthcareProviders, setHealthcareProviders] = useState([]); // change later to [] after api is set up 
    const[related_county, setRelatedCounty] = useState<any>(null);
    const[related_healthcare_providers, setRelatedHealthcareProviders] = useState<any>([]);

    useEffect(() => {
        client.get("coal_mine/"+params.id.toString()).then((response) => {
            setCoalMine(response.data);
        })
        client.get("counties").then((response) => {
            setCounties(response.data);
        })
        client.get("healthcare_providers").then((response) => {
            setHealthcareProviders(response.data);
        })
    }, [])

    useEffect(() => {
        if(coal_mine !== undefined && coal_mine !== null) {
            setRelatedCounty(getCounty(coal_mine.close_county, counties));
            setRelatedHealthcareProviders(getHealthcareProviders(coal_mine.close_healthcare_providers, healthcareProviders));
        }
    }, [coal_mine, counties, healthcareProviders])

    if (coal_mine === null || counties.length == 0 || healthcareProviders.length == 0 || related_county == null || related_county.idx == undefined) {
        return (
            // <Container>
            //     <h1>Coal Mine Not Found</h1>
            // </Container>
            <div></div>
        )
    }

    return (
        <Container>
            <CoalMineInstance 
            title={coal_mine.mineName}
            prop1={related_county.county}
            prop2={coal_mine.coalRankDescription}
            prop3={coal_mine.average_employees}
            prop4={coal_mine.mineStatusDescription}
            prop5={coal_mine.production.toString()}
            prop6={related_healthcare_providers.map(function(related_healthcare_provider : any) {return related_healthcare_provider.name.toString();}).join(', ')}
            imgSrc={findUrlByName(coal_mine.mineName)}
            mapSrc={'https://www.google.com/maps/embed/v1/place?key=AIzaSyDSFN1kt4StJE_0zKTdX8uC9-T8489W-2w&q=' + coal_mine.latitude + "," + coal_mine.longitude}></CoalMineInstance>

            <Row>
                <Col className='ms-4'>
                    <Card className="ms-auto me-auto mb-4 mt-4">
                            <Card.Header>Counties:</Card.Header>
                            <Card.Body>
                                <Link key={related_county.idx} href={'../counties/' + related_county.idx.toString()}>
                                    <Button className='w-100 mb-4'>
                                        {related_county.county}
                                    </Button>
                                </Link> 
                            </Card.Body>
                    </Card>
                </Col>
                <Col className="me-4">
                    <Card className="ms-auto me-auto mb-4 mt-4">
                        <Card.Header>Healthcare Providers:</Card.Header>
                        <Card.Body>
                           
                            {related_healthcare_providers.map((healthcare_provider : any) => {
                                return (
                                    <Link key={healthcare_provider.idx} href={'../healthcare-providers/' + healthcare_provider.idx.toString()}>
                                        <Button className='w-100 mb-4'>
                                            {healthcare_provider.name}
                                        </Button>
                                    </Link> 
                                )
                            })} 
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )


}

export default CoalMinePage;