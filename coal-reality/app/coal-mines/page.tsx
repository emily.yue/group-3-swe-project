'use client'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';  
import CoalMineCard from './CoalMineCard'
import PaginationBar from '../components/paginationbar';
import FilterDropdown from '../components/filterdropdown';
import SearchBar from '../components/search-bar';
import { search_model } from '../utils/api';

// change later
import coal_mine_img from '../images/coal_mine_img.json'

function findUrlByName(name: string): string {
  const entry = coal_mine_img.find(item => item.name === name);
  if (entry == null) {
      return ""
  }
  return entry.url;
}

const client = axios.create( {
  headers: {
    'Content-Type' : 'application/json',
  },
  baseURL: "https://api.coal-reality.me/api"  
  // baseURL:  "http://127.0.0.1:5000/api/"
});


const cards_per_page = 10;

// filtering options
const mine_status = ["New, under construction", "Active, men working, not producing", "Temporarily closed", "Permanently abandoned", "Active"]
const sort_by_map: Record<string, string> = { 
  'Mine Name': 'mineName',
  'Average Employees': 'average_employees',
  'Production': 'production'
}
const coal_rank = ["Preparation Plant", "Anthracite", "Bituminous"]

const CoalMines = () => {
  const[coalMines, setCoalMines] = useState<any[]>([]); // change later to [] after api is set up
  const[counties, setCounties] = useState([]); // change later to [] after api is set up
  const[healthcareProviders, setHealthcareProviders] = useState([]); // change later to [] after api is set up
  const[numCoalMines, setNumCoalMines] = useState(coalMines.length);
  const[currentPage, setCurrentPage] = useState(1);
  const[totalNumPages, setTotalNumPages] = useState(1);

  // filtering states
  const[mineStatusFilter, setMineStatusFilter] = useState("");
  const[sortByProperty, setSortByProperty] = useState("");
  const[coalRankFilter, setCoalRankFilter] = useState("");

  // highlighting
  // const[highlightText, setHighlightText] = useState("");

  const handleSearch = async (search_term : any) => {
    const search_data = await search_model(search_term, "coal_mines");
    // this is data retrieved
    // get highlight term from here too
    // const { highlight_term } = search_data;
    // setHighlightText(highlight_term);
    setCoalMines(search_data);
    setTotalNumPages(1);
  }

  useEffect(() => {
    client.get("counties").then((response) => {
      setCounties(response.data);
    })
    client.get("healthcare_providers").then((response) => {
      setHealthcareProviders(response.data);
    })
  }, [])
  
  useEffect(() => {
    // client.get("coal_mines/"+ currentPage.toString()).then((response) => {
    //   setCoalMines(response.data.slice(1, 11));
    //   setTotalNumPages(response.data[0]); 
    // })
    const mineStatusURL = mineStatusFilter === "" ? "" : "&mineStatusDescription="+mineStatusFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by="+sort_by_map[sortByProperty];
    const coalRankURL = coalRankFilter === "" ? "" : "&coalRankDescription="+coalRankFilter;
    client.get("coal_mines/" + currentPage + "?" + mineStatusURL + sortByURL + coalRankURL).then((response) => {
      console.log("coal_mines/" + currentPage + "?" + mineStatusURL + sortByURL + coalRankURL)
      setCoalMines(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
      console.log(coalMines)
    })
    // this is where we will get all counties
  }, [currentPage])

  useEffect(() => {
    setNumCoalMines(coalMines.length);
  }, [coalMines])

  const prevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  }

  const nextPage = () => {  
    if (currentPage < totalNumPages) {
      setCurrentPage(currentPage + 1);
    }
  }

  const getCountyName = (close_county: number) => {
    let county : any = counties.find((county : any) => county.idx === close_county)
    if(county !== undefined){
      return county.county;
    }
    return ""
  }

  const getHealthcareProviderNames = (close_healthcare_providers: number[]) => {
    if(close_healthcare_providers == undefined){
      return [];
    }
    let healthcare_provider_names: string[] = [];
    close_healthcare_providers.forEach((healthcare_provider_idx) => {
      let healthcare_provider : any = healthcareProviders.find((healthcare_provider : any) => healthcare_provider.idx === healthcare_provider_idx)
      if (healthcare_provider !== undefined) {
        healthcare_provider_names.push(healthcare_provider.name);
      }
    })
    return healthcare_provider_names.slice(0, 5);
  }

  function filterApi() {
    setCurrentPage(1)
    // url add ons for all the filters/sorts
    const mineStatusURL = mineStatusFilter === "" ? "" : "&mineStatusDescription="+mineStatusFilter;
    const sortByURL = sortByProperty === "" ? "" : "&sort_by="+sort_by_map[sortByProperty];
    const coalRankURL = coalRankFilter === "" ? "" : "&coalRankDescription="+coalRankFilter;
    // get new coal mines based on filter
    client.get("coal_mines/" + currentPage + "?" + mineStatusURL + sortByURL + coalRankURL).then((response) => {
      setCoalMines(response.data.slice(1, 11));
      setTotalNumPages(response.data[0]); 
    })
  
  }

  // if filter resulted in no results, set healthcareProviders to empty array
  if (coalMines.length === 1 && coalMines[0].length === 0) {
    setCoalMines([])
    setCurrentPage(0)
  }
  return (
    <Container>

      <Container className="container text-center mt-5 mb-4">
        <h1>Coal Mines</h1>
        <p>{numCoalMines} Coal Mines</p>
      </Container>

      <Container className="container text-center mt-5 mb-4">
        <Row>
        <SearchBar model="Coal Mines" handleSearch={handleSearch}/>
          <Col>
            <FilterDropdown 
            name="Filter by Mine Status Description" 
            id="mine-status-filter" 
            options={mine_status} 
            setFilter={setMineStatusFilter}></FilterDropdown>
          </Col>
          <Col>
            <FilterDropdown
              name='Filter by Coal Rank Description'
              id='coal-rank-filter'
              options={coal_rank}
              setFilter={setCoalRankFilter}></FilterDropdown>
          </Col>
          <Col> 
            <FilterDropdown
            name='Sort by ...'
            id='sort-by-name'
            options={['Mine Name', 'Average Employees', 'Production']}
            setFilter={setSortByProperty}></FilterDropdown>
          </Col>
        </Row>
        <Row className='mt-4 '>
          <Col></Col>
          <Col><button className="btn btn-primary size-small"onClick={filterApi}>Apply</button></Col>
          <Col></Col>
        </Row>
      </Container>

      <Container> 
        <PaginationBar currentPage={currentPage} totalNumPages={totalNumPages} prevPage={prevPage} nextPage={nextPage} lastPage={() => setCurrentPage(totalNumPages)} firstPage={() => setCurrentPage(1)} setPage={setCurrentPage}></PaginationBar> 
        <Row
          xl={3}
          lg={3}
          md={3}
          sm={2}
          xs={1}
          className="d-flex g-1 p-1"
        >
          {coalMines.map((coal_mine : any) => {
            return (
                <Col key={coal_mine.idx} className="d-flex align-items-stretch">
                  <CoalMineCard
                    title={coal_mine.mineName}
                    prop1={getCountyName(coal_mine.close_county)}
                    prop2={coal_mine.coalRankDescription}
                    prop3={coal_mine.average_employees}
                    prop4={coal_mine.mineStatusDescription}
                    prop5={coal_mine.production}
                    prop6={getHealthcareProviderNames(coal_mine.close_healthcare_providers).join(", ")}
                    imgSrc={findUrlByName(coal_mine.mineName)}
                    coal_mine_link={`/coal-mines/${coal_mine.idx}`}
                    highlight_term={coal_mine.hasOwnProperty('match_str') ? coal_mine.match_str[0] : ""}
                    ></CoalMineCard>
                </Col>
            )
          })}    
        </Row>  
      </Container>  

    </Container>
  )
}

export default CoalMines