'use client'
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card' 
import Link from 'next/link'
import "./CoalMineCard.css"
import { Button } from 'react-bootstrap'
import { highlightText } from '../utils/highlight';


const CoalMineCard = ({title, 
                      prop1, 
                      prop2, 
                      prop3, 
                      prop4, 
                      prop5, 
                      prop6,
                      imgSrc,
                      coal_mine_link,
                      highlight_term
                    } : {
                      title: string; 
                      prop1: string; prop2: string; prop3: any; prop4: string; prop5: string; prop6: string;
                      imgSrc: string;
                      coal_mine_link: string;
                      highlight_term: string
                    }) => {

    return (
        <Card className="ms-3 me-3 mb-4 mt-4" data-testid="coal-mine-card">
            <Card.Img variant='top' src={imgSrc} alt="Card image cap" />
            <Card.Body>
                <Card.Title>{highlightText(title, highlight_term)}</Card.Title>
                <Card.Text>
                    <b>County:</b> {highlightText(prop1, highlight_term)}
                    <br/>
                    <b>Coal Rank Description:</b> {highlightText(prop2, highlight_term)}
                    <br/>
                    <b>Average Employees:</b> {highlightText(prop3, highlight_term)}
                    <br/>
                    <b>Mine Status Description:</b> {highlightText(prop4, highlight_term)}
                    <br/>
                    <b>Production:</b> {highlightText(prop5, highlight_term)}
                    <br/>
                    <b>Healthcare Providers Nearby:</b> {highlightText(prop6, highlight_term)}
                </Card.Text>
            </Card.Body>
            <Card.Footer className="text-center">
                <Link href={coal_mine_link}>
                    <Button>Learn More</Button>
                </Link>
                <br/>
            </Card.Footer>
        </Card>
    )

}

export default CoalMineCard