// Jest Frontend Unit Tests

import React, { useState, useEffect } from 'react';
import { BrowserRouter } from "react-router-dom";

import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import HomePage from "../app/page";
import AboutPage from "../app/about/page";
import Navigation from "../app/components/navbar";
import CountyCard from "../app/counties/CountiesCard";
import HealthcareProviderCard from "../app/healthcare-providers/HealthCareCard";
import CoalMineCard from "../app/coal-mines/CoalMineCard";

test("Testing Home Page", () => {
  render(<HomePage />, { wrapper: BrowserRouter });
  expect(screen.getByText("This is the coal reality.")).toBeInTheDocument();
  expect(screen.getByText("What is the coal reality?")).toBeInTheDocument();
});

test("Testing Home Buttons", () => {
  render(<HomePage />, { wrapper: BrowserRouter });
  expect(screen.getByText("Coal Mines")).toBeInTheDocument();
  expect(screen.getByText("Healthcare Providers")).toBeInTheDocument();
  expect(screen.getByText("Counties")).toBeInTheDocument();
});

test("Testing About Page", () => {
  render(<AboutPage />, { wrapper: BrowserRouter });
  expect(screen.getByText("About Appalachia's Coal Reality")).toBeInTheDocument();
});

test("Testing Gitlab Stats Rendering in About Page", () => {
  render(<AboutPage />, { wrapper: BrowserRouter });
  expect(screen.getByText("Our Team")).toBeInTheDocument();
  expect(screen.getByText("Total GitLab Statistics")).toBeInTheDocument();
});

test("Testing Tool and API information Rendering in About Page", () => {
  render(<AboutPage />, { wrapper: BrowserRouter });
  expect(screen.getByText("Tools")).toBeInTheDocument();
  expect(screen.getByText("API")).toBeInTheDocument();
});

test("Testing Navigation", () => {
  render(<Navigation />, { wrapper: BrowserRouter });
  expect(screen.getByText("Home")).toBeInTheDocument();
  expect(screen.getByText("About")).toBeInTheDocument();
  expect(screen.getByText("Coal Mines")).toBeInTheDocument();
  expect(screen.getByText("Counties")).toBeInTheDocument();
  expect(screen.getByText("Healthcare Providers")).toBeInTheDocument();
});

test("Testing County Card", () => {
  render(<CountyCard />, { wrapper: BrowserRouter });
  expect(screen.getByTestId("county-card")).toBeInTheDocument();
});

test("Testing Healthcare Provider Card", () => {
  render(<HealthcareProviderCard />, { wrapper: BrowserRouter });
  expect(screen.getByTestId("healthcare-provider-card")).toBeInTheDocument();
});

test("Testing Coal Mine Card", () => {
  render(<CoalMineCard />, { wrapper: BrowserRouter });
  expect(screen.getByTestId("coal-mine-card")).toBeInTheDocument();
});

test("Testing Developer Card", () => {
  render(<AboutPage />, { wrapper: BrowserRouter });
  expect(screen.getByText("Role:")).toBeInTheDocument();
  expect(screen.getByText("About me:")).toBeInTheDocument();
  expect(screen.getByText("Commits:")).toBeInTheDocument();
  expect(screen.getByText("Issues:")).toBeInTheDocument();
  expect(screen.getByText("Unit Tests:")).toBeInTheDocument();

});
