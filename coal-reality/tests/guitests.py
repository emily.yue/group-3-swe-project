# Selenium Frontend Acceptance Tests

import unittest
import time
from selenium import webdriver
from selenium import common
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service

from webdriver_manager.chrome import ChromeDriverManager

# If locally testing, change URL to "http://localhost:3000/"
# URL = "http://localhost:3000/"
URL = "https://www.coal-reality.me/"

class Test(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=chrome_options, service=Service(ChromeDriverManager().install())) # webdriver.Chrome(options=chrome_options)
        self.driver.get(URL)
        self.wait = WebDriverWait(self.driver, 10)

    def tearDown(self):
        # self.driver.close()
        pass

    def navigate_to_page(self, link_text):
        link = self.driver.find_element(By.LINK_TEXT, link_text)
        link.click()
        link_href = link.get_attribute('href')
        self.wait.until(EC.url_to_be(link_href))

    def testTitle(self):
        self.assertEqual(self.driver.title, "Appalachia Coal Reality", "Page title did not match expected title.")

    def testNavbarHome(self):
        self.navigate_to_page("Home")
        self.assertEqual(self.driver.current_url, URL, "Navigating to Home did not redirect to the expected URL.")

    def testNavbarAbout(self):
        self.navigate_to_page("About")
        WebDriverWait(self.driver, 10).until(EC.url_to_be(URL + "about"));
        self.assertEqual(self.driver.current_url, URL  + "about", "Navigating to About did not redirect to the expected URL.")

    def testNavbarCounties(self):
        self.navigate_to_page("Counties")
        self.assertEqual(self.driver.current_url, URL  + "counties", "Navigating to Counties did not redirect to the expected URL.")

    def testNavbarHealthCareProviders(self):
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL  + "healthcare-providers", "Navigating to Healthcare Providers did not redirect to the expected URL.")

    def testNavbarCoalMines(self):
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL  + "coal-mines", "Navigating to Coal Mines did not redirect to the expected URL.") 

    def test_multi_navigation_coal_to_healthcare(self):
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL + "coal-mines", "Navigating to Coal Mines did not redirect to the expected URL.")
        
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers", "Navigating to Healthcare Providers did not redirect to the expected URL.")
        
        self.driver.back()
        self.driver.refresh()
        self.assertEqual(self.driver.current_url, URL + "coal-mines", "Going back did not redirect to the Coal Mines page.")
    
    def test_multi_navigation_home_to_about(self):
        self.navigate_to_page("About")
        self.assertEqual(self.driver.current_url, URL + "about", "Navigating to about did not redirect to the expected URL.")
        
        self.navigate_to_page("Home")
        self.assertEqual(self.driver.current_url, URL, "Navigating to Home did not redirect to the expected URL.")
        
        self.driver.back()
        self.driver.refresh()
        self.assertEqual(self.driver.current_url, URL + "about", "Going back did not redirect to the About page.")

    def test_multi_navigation_home_to_coal(self):
        self.navigate_to_page("Home")
        self.assertEqual(self.driver.current_url, URL, "Navigating to about did not redirect to the expected URL.")
        
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL + "coal-mines", "Navigating to Coal Mines did not redirect to the expected URL.")
        
        self.driver.back()
        self.driver.refresh()
        self.assertEqual(self.driver.current_url, URL, "Going back did not redirect to the About page.")

    def test_multi_navigation_home_to_healthcare(self):
        self.navigate_to_page("Home")
        self.assertEqual(self.driver.current_url, URL, "Navigating to about did not redirect to the expected URL.")
        
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers", "Navigating to Healthcare Providers did not redirect to the expected URL.")
        
        self.driver.back()
        self.driver.refresh()
        self.assertEqual(self.driver.current_url, URL, "Going back did not redirect to the About page.")

    def test_multi_navigation_home_to_counties(self):
        self.navigate_to_page("Home")
        self.assertEqual(self.driver.current_url, URL, "Navigating to about did not redirect to the expected URL.")
        
        self.navigate_to_page("Counties")
        self.assertEqual(self.driver.current_url, URL + "counties", "Navigating to Counties did not redirect to the expected URL.")
        
        self.driver.back()
        self.driver.refresh()
        self.assertEqual(self.driver.current_url, URL, "Going back did not redirect to the About page.")
    
    def testCoalMineFilter(self):
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL + "coal-mines")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'mine-status-filter')))
        children = elem.find_elements(By.CSS_SELECTOR,'#mine_status > option[value="Active"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "coal-mines")

    def testCountiesFilter(self):
        self.navigate_to_page("Counties")
        self.assertEqual(self.driver.current_url, URL + "counties")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'state-filter')))
        children = elem.find_elements(By.CSS_SELECTOR,'#state-filter > option[value="Alabama"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "counties")

    def testHealthcareProvidersFilter(self):
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'county-filter')))
        children = elem.find_elements(By.CSS_SELECTOR,'#county-filter > option[value="Bell"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")

    def testCoalMinesSort(self):
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL + "coal-mines")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'sort-by-name')))
        children = elem.find_element(By.CSS_SELECTOR,'#sort-by-name > option[value="Mine Name"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "coal-mines")

    def testCountiesSort(self):
        self.navigate_to_page("Counties")
        self.assertEqual(self.driver.current_url, URL + "counties")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'sort-by-name')))
        children = elem.find_element(By.CSS_SELECTOR,'#sort-by-name > option[value="County Name"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "counties")

    def testHealthcareProvidersSort(self):
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")
        elem = self.wait.until(EC.element_to_be_clickable((By.ID, 'sort-by-name')))
        children = elem.find_element(By.CSS_SELECTOR,'#sort-by-name > option[value="Street Address"]')
        update = self.driver.find_element(By.XPATH, "//button[text()='Apply']")
        self.driver.execute_script("arguments[0].click();", update)
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")

    def testSitewideSearch(self):
        self.assertEqual(self.driver.current_url, URL)
        search = self.driver.find_element(By.XPATH, "//input[@placeholder='Search']")
        # search.send_keys("Coal Mine")
        # search.send_keys(Keys.RETURN)
        self.assertEqual(self.driver.current_url, URL) 

    def testCoalMinesSearch(self):
        self.navigate_to_page("Coal Mines")
        self.assertEqual(self.driver.current_url, URL + "coal-mines")
        # search = self.driver.find_element(By.XPATH, "//input[@placeholder='Search Coal Mines']")
        # search.send_keys("Coal Mine")
        # search.send_keys(Keys.RETURN)
        self.assertEqual(self.driver.current_url, URL + "coal-mines")

    def testContiesSearch(self):
        self.navigate_to_page("Counties")
        self.assertEqual(self.driver.current_url, URL + "counties")
        # search = self.driver.find_element(By.XPATH, "//input[@placeholder='Search Counties']")
        # search.send_keys("Coal Mine")
        # search.send_keys(Keys.RETURN)
        self.assertEqual(self.driver.current_url, URL + "counties")

    def testHeathcareProvidersSearch(self):
        self.navigate_to_page("Healthcare Providers")
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")
        # search = self.driver.find_element(By.XPATH, "//input[@placeholder='Search Healthcare Providers']")
        # search.send_keys("Coal Mine")
        # search.send_keys(Keys.RETURN)
        self.assertEqual(self.driver.current_url, URL + "healthcare-providers")



if __name__ == "__main__":
    unittest.main()
